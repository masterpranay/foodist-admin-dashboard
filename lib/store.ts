import { configureStore } from "@reduxjs/toolkit";
import { loginReducer } from "./features/loginSlice";
import { restaurantReducer } from "./features/restaurantSlice";
import { dishReducer } from "./features/dishSlice";
import { orderReducer } from "./features/orderSlice";
import { ThunkDispatch } from "@reduxjs/toolkit";
import { userReducer } from "./features/usersSlice";
import locationReducer from "./features/locationSlice";

export const makeStore = () => {
  return configureStore({
    reducer: {
      login: loginReducer,
      restaurant: restaurantReducer,
      dish: dishReducer,
      order: orderReducer,
      user: userReducer,
      location: locationReducer,
    },
  });
};

export type AppStore = ReturnType<typeof makeStore>;
export type RootState = ReturnType<AppStore["getState"]>;
export type AppDispatch = AppStore["dispatch"];
export type AppThunkDispatch = AppDispatch & ThunkDispatch<RootState, any, any>;
