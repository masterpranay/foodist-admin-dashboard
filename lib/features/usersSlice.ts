import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
const APIURL = process.env.NEXT_PUBLIC_API_URL;

interface IUser {
  id: string;
  name: string;
  mobileNumber: string;
  address: string;
}

interface IUserInitialState {
  users?: IUser[];
  usersStatus?: "idle" | "loading" | "success" | "failed";
}

const saveToLocalStorage = (state: IUserInitialState) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem("user", serializedState);
  } catch (error) {
    console.log(error);
  }
};

const userInitialState: IUserInitialState = {
  users: [],
  usersStatus: "idle",
};

export const fetchAllUsers = createAsyncThunk("fetchAllUsers", async () => {
  let usersId = null;
  try {
    const res = await fetch(`${APIURL}/api/users/get-all-users-id`);
    const resData = await res.json();
    usersId = resData;
  } catch (error: any) {
    console.log(error.message);
    throw new Error(error.message);
  }

  let users = null;
  let usersRes = null;
  try {
    usersRes = usersId.map(async (userId: string) => {
      const res = await fetch(`${APIURL}/api/users/get-user-by-id/${userId}`);
      const resData = await res.json();
      return {
        id: resData.id,
        name: resData.name,
        mobileNumber: resData.mobileNumber,
        address: resData.address,
      };
    });
  } catch (error: any) {
    console.log(error.message);
    throw new Error(error.message);
  }
  users = await Promise.all(usersRes);
  console.log(users);
  return { status: "success", users: users };
});

const userSlice = createSlice({
  name: "user",
  initialState: userInitialState,
  reducers: {
    updateUser: (state, action) => {
      saveToLocalStorage({
        ...state,
        users: action.payload,
      });
      return {
        ...state,
        users: action.payload,
      };
    },
    resetUser: (state) => {
      return {
        ...state,
        users: [],
        usersStatus: "idle",
      };
    },
    fetchUsersFromLocalStorage: (state) => {
      const localState = localStorage.getItem("user");
      if (localState) {
        return JSON.parse(localState);
      }
      return state;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchAllUsers.pending, (state, action) => {
        return {
          ...state,
          usersStatus: "loading",
        };
      })
      .addCase(fetchAllUsers.fulfilled, (state, action) => {
        saveToLocalStorage({
          ...state,
          users: action.payload.users,
          usersStatus: "success",
        });
        return {
          ...state,
          users: action.payload.users,
          usersStatus: "success",
        };
      })
      .addCase(fetchAllUsers.rejected, (state, action) => {
        return {
          ...state,
          usersStatus: "failed",
        };
      });
  },
});

export const { updateUser, resetUser, fetchUsersFromLocalStorage } =
  userSlice.actions;
export const userReducer = userSlice.reducer;
