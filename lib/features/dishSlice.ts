import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
const APIURL = process.env.NEXT_PUBLIC_API_URL;

interface IDish {
  id: string;
  name: string;
  description: string;
  price: number;
  image: string;
  type: "veg" | "non-veg";
  category: string;
}

interface IDishInitialState {
  dishes: IDish[];
  status: string;
  addStatus?: string;
  updateStatus?: string;
  deleteStatus?: string;
}

const saveToLocalStorage = (state: IDishInitialState) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem("dish", serializedState);
  } catch (error) {
    console.log(error);
  }
};

const dishInitialState: IDishInitialState = {
  dishes: [],
  status: "idle",
};

export const fetchDishes = createAsyncThunk(
  "fetchDishes",
  async (
    data: {
      restaurantId: string;
    },
    thunkApi
  ) => {
    try {
      let dishes = null;
      console.log(data.restaurantId);
      try {
        const res = await fetch(
          `${APIURL}/api/dishes/get-all-dishes-id/${data.restaurantId}`
        );
        const resData = await res.json();

        dishes = resData.map(async (dish: any) => {
          const dishRes = await fetch(
            `${APIURL}/api/dishes/get-dish-by-id/${dish.id}`
          );
          const dishResData = await dishRes.json();
          return {
            id: dishResData.id,
            name: dishResData.name,
            description: dishResData.description,
            price: dishResData.price,
            image: dishResData.image,
            type: dishResData.type,
            category: dishResData.category,
          };
        });
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }
      dishes = await Promise.all(dishes);
      return { status: "success", dishes: dishes };
    } catch (error: any) {
      console.log(error.message);
      return thunkApi.rejectWithValue({ status: error.message });
    }
  }
);

export const addDish = createAsyncThunk(
  "addDish",
  async (
    data: {
      restaurantId: string;
      name: string;
      description: string;
      price: number;
      image: string;
      type: "veg" | "non-veg";
      category: string;
    },
    thunkApi
  ) => {
    try {
      let dish = null;
      try {
        const res = await fetch(`${APIURL}/api/dishes/create-dish`, {
          method: "POST",
          body: JSON.stringify({
            restaurant: data.restaurantId,
            name: data.name,
            description: data.description,
            price: data.price,
            image: data.image,
            type: data.type,
            category: data.category,
          }),
          headers: {
            "Content-Type": "application/json",
          },
        });
        const resData = await res.json();
        dish = {
          id: resData.id,
          name: resData.name,
          description: resData.description,
          price: resData.price,
          image: resData.image,
          type: resData.type,
          category: resData.category,
        };
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }
      return { status: "success", dish };
    } catch (error: any) {
      return thunkApi.rejectWithValue({ status: error.message });
    }
  }
);

export const updateDish = createAsyncThunk(
  "updateDish",
  async (
    data: {
      id: string;
      name: string;
      description: string;
      price: number;
      image: string;
      type: "veg" | "non-veg";
      category: string;
    },
    thunkApi
  ) => {
    try {
      let dish = null;
      try {
        const res = await fetch(`${APIURL}/api/dishes/update-dish/${data.id}`, {
          method: "PATCH",
          body: JSON.stringify({
            name: data.name,
            description: data.description,
            price: data.price,
            image: data.image,
            type: data.type,
            category: data.category,
          }),
          headers: {
            "Content-Type": "application/json",
          },
        });
        const resData = await res.json();
        dish = {
          id: resData.id,
          name: resData.name,
          description: resData.description,
          price: resData.price,
          image: resData.image,
          type: resData.type,
          category: resData.category,
        };
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }
      return { status: "success", dish };
    } catch (error: any) {
      return thunkApi.rejectWithValue({ status: error.message });
    }
  }
);

export const deleteDish = createAsyncThunk(
  "deleteDish",
  async (
    data: {
      id: string;
    },
    thunkApi
  ) => {
    try {
      try {
        const res = await fetch(`${APIURL}/api/dishes/delete-dish/${data.id}`, {
          method: "DELETE",
        });
        const resData = await res.json();
        console.log(resData);
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }
      return { status: "success", id: data.id };
    } catch (error: any) {
      return thunkApi.rejectWithValue({ status: error.message });
    }
  }
);

const dishSlice = createSlice({
  name: "dish",
  initialState: dishInitialState,
  reducers: {
    updateDishes: (state, action) => {
      saveToLocalStorage({
        ...state,
        dishes: action.payload,
      });

      return {
        ...state,
        dishes: action.payload,
      };
    },
    resetDishes: (state) => {
      return {
        ...state,
        dishes: [],
        status: "idle",
        addStatus: undefined,
        updateStatus: undefined,
      };
    },
    fetchDishesFromLocalStorage: (state) => {
      const localState = localStorage.getItem("dish");
      if (localState) {
        return JSON.parse(localState);
      }
      return state;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchDishes.pending, (state, action) => {
        return {
          ...state,
          status: "loading",
        };
      })
      .addCase(fetchDishes.fulfilled, (state, action) => {
        saveToLocalStorage({
          ...state,
          dishes: action.payload.dishes,
          status: action.payload.status,
        });
        return {
          ...state,
          dishes: action.payload.dishes,
          status: action.payload.status,
        };
      })
      .addCase(fetchDishes.rejected, (state, action) => {
        return {
          ...state,
          status: "failed",
        };
      })
      .addCase(addDish.pending, (state, action) => {
        return {
          ...state,
          addStatus: "loading",
        };
      })
      .addCase(addDish.fulfilled, (state, action) => {
        saveToLocalStorage({
          ...state,
          dishes: [...state.dishes, action.payload.dish],
          addStatus: "success",
        });
        return {
          ...state,
          dishes: [...state.dishes, action.payload.dish],
          addStatus: "success",
        };
      })
      .addCase(addDish.rejected, (state, action) => {
        return {
          ...state,
          addStatus: "failed",
        };
      })
      .addCase(updateDish.pending, (state, action) => {
        return {
          ...state,
          updateStatus: "loading",
        };
      })
      .addCase(updateDish.fulfilled, (state, action) => {
        saveToLocalStorage({
          ...state,
          dishes: state.dishes.map((dish) => {
            if (dish.id === action.payload.dish.id) {
              return action.payload.dish;
            }
            return dish;
          }),
          updateStatus: "success",
        });
        return {
          ...state,
          dishes: state.dishes.map((dish) => {
            if (dish.id === action.payload.dish.id) {
              return action.payload.dish;
            }
            return dish;
          }),
          updateStatus: "success",
        };
      })
      .addCase(updateDish.rejected, (state, action) => {
        return {
          ...state,
          updateStatus: "failed",
        };
      })
      .addCase(deleteDish.pending, (state, action) => {
        return {
          ...state,
          deleteStatus: "loading",
        };
      })
      .addCase(deleteDish.fulfilled, (state, action) => {
        saveToLocalStorage({
          ...state,
          dishes: state.dishes.filter((dish) => dish.id !== action.payload.id),
          deleteStatus: "success",
        });
        return {
          ...state,
          dishes: state.dishes.filter((dish) => dish.id !== action.payload.id),
          deleteStatus: "success",
        };
      })
      .addCase(deleteDish.rejected, (state, action) => {
        return {
          ...state,
          deleteStatus: "failed",
        };
      });
  },
});

export const { updateDishes, resetDishes, fetchDishesFromLocalStorage } = dishSlice.actions;
export const dishReducer = dishSlice.reducer;
