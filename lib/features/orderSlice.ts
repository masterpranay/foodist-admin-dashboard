import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
const APIURL = process.env.NEXT_PUBLIC_API_URL;

interface IOrder {
  id: string;
  dishes: {
    id: string;
    name: string;
    quantity: number;
  }[];
  restaurantId: string;
  restaurant: string;
  userId: string;
  user: string;
  mobileNumber: string;
  amount: number;
  status: "pending" | "accepted" | "rejected" | "delivered" | "delivered";
  createdAt: string;
}

interface IOrderState {
  orders: IOrder[];
  userOrders: IOrder[];
  status: "idle" | "loading" | "failed" | "success";
  userOrdersStatus?: "idle" | "loading" | "failed" | "success";
  restaurantOrders: IOrder[];
  restaurantOrdersStatus?: "idle" | "loading" | "failed" | "success";
}

const saveToLocalStorage = (state: IOrderState) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem("order", serializedState);
  } catch (error) {
    console.log(error);
  }
};

const initialState: IOrderState = {
  orders: [],
  userOrders: [],
  status: "idle",
  userOrdersStatus: "idle",
  restaurantOrders: [],
  restaurantOrdersStatus: "idle",
};

export const fetchOrders = createAsyncThunk(
  "orders/fetchOrders",
  async (
    data: {
      restaurantId: string;
    },
    thunkApi
  ) => {
    try {
      let ordersId = null;
      try {
        const res = await fetch(
          `${APIURL}/api/orders/get-orders-restaurant/${data.restaurantId}`
        );
        const resData = await res.json();
        ordersId = resData;
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }

      let orders: any = null;

      try {
        orders = ordersId.map(async (orderId: any) => {
          const res = await fetch(
            `${APIURL}/api/orders/get-order/${orderId.id}`
          );
          const resData = await res.json();
          return resData;
        });
        orders = await Promise.all(orders);
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }

      let dishesName: {
        name: string;
        quantity: number;
      }[][] = [];

      try {
        let dishesNameRes = orders.map(async (order: any) => {
          let tempDishesRes = [];
          tempDishesRes = order.dish.map(async (id: string) => {
            const res = await fetch(
              `${APIURL}/api/dishes/get-dish-by-id/${id}`
            );
            const resData = await res.json();
            return {
              id: resData.id,
              name: resData.name,
              quantity: order.itemQuantity[id],
            };
          });
          tempDishesRes = await Promise.all(tempDishesRes);
          return tempDishesRes;
        });

        dishesName = await Promise.all(dishesNameRes);
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }

      let userInfo: {
        id: string;
        mobileNumber: string;
        name: string;
      }[] = [];

      try {
        let userInfoRes = orders.map(async (order: any) => {
          const res = await fetch(
            `${APIURL}/api/users/get-user-by-id/${order.user}`
          );
          const resData = await res.json();
          return {
            id: resData.id,
            mobileNumber: resData.mobileNumber,
            name: resData.name || "No Name",
          };
        });
        userInfo = await Promise.all(userInfoRes);
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }

      let restaurantInfo: {
        id: string;
        name: string;
      }[] = [];

      try {
        let restaurantInfoRes = orders.map(async (order: any) => {
          const res = await fetch(
            `${APIURL}/api/restaurants/get-restaurant-by-id/${order.restaurant}`
          );
          const resData = await res.json();
          return {
            id: resData.id,
            name: resData.name,
          };
        });
        restaurantInfo = await Promise.all(restaurantInfoRes);
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }

      orders = orders.map((order: any, index: any) => {
        return {
          id: order.id,
          dishes: dishesName[index],
          restaurant: restaurantInfo[index].name,
          restaurantId: restaurantInfo[index].id,
          userId: userInfo[index].id,
          user: userInfo[index].name,
          mobileNumber: userInfo[index].mobileNumber,
          amount: order.totalPrice,
          status: order.status,
          createdAt: order.created,
        };
      });

      return { status: "success", orders: orders };
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.message);
    }
  }
);

export const fetchUserOrders = createAsyncThunk(
  "orders/fetchUserOrders",
  async (
    data: {
      userId: string;
    },
    thunkApi
  ) => {
    try {
      let ordersId = null;
      try {
        const res = await fetch(
          `${APIURL}/api/orders/get-orders-user/${data.userId}`
        );
        const resData = await res.json();
        ordersId = resData;
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }

      let orders: any = null;

      try {
        orders = ordersId.map(async (orderId: any) => {
          const res = await fetch(
            `${APIURL}/api/orders/get-order/${orderId.id}`
          );
          const resData = await res.json();
          return resData;
        });
        orders = await Promise.all(orders);
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }

      let dishesName: {
        name: string;
        quantity: number;
      }[][] = [];

      try {
        let dishesNameRes = orders.map(async (order: any) => {
          let tempDishesRes = [];
          tempDishesRes = order.dish.map(async (id: string) => {
            const res = await fetch(
              `${APIURL}/api/dishes/get-dish-by-id/${id}`
            );
            const resData = await res.json();
            return {
              id: resData.id,
              name: resData.name,
              quantity: order.itemQuantity[id],
            };
          });
          tempDishesRes = await Promise.all(tempDishesRes);
          return tempDishesRes;
        });

        dishesName = await Promise.all(dishesNameRes);
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }

      let restaurantInfo: {
        id: string;
        name: string;
      }[] = [];

      try {
        let restaurantInfoRes = orders.map(async (order: any) => {
          const res = await fetch(
            `${APIURL}/api/restaurants/get-restaurant-by-id/${order.restaurant}`
          );
          const resData = await res.json();
          return {
            id: resData.id,
            name: resData.name,
          };
        });
        restaurantInfo = await Promise.all(restaurantInfoRes);
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }

      let userInfo: {
        id: string;
        mobileNumber: string;
        name: string;
      }[] = [];

      try {
        let userInfoRes = orders.map(async (order: any) => {
          const res = await fetch(
            `${APIURL}/api/users/get-user-by-id/${order.user}`
          );
          const resData = await res.json();
          return {
            id: resData.id,
            mobileNumber: resData.mobileNumber,
            name: resData.name || "No Name",
          };
        });
        userInfo = await Promise.all(userInfoRes);
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }

      orders = orders.map((order: any, index: any) => {
        return {
          id: order.id,
          dishes: dishesName[index],
          restaurant: restaurantInfo[index].name,
          restaurantId: restaurantInfo[index].id,
          userId: userInfo[index].id,
          user: userInfo[index].name,
          mobileNumber: userInfo[index].mobileNumber,
          amount: order.totalPrice,
          status: order.status,
          createdAt: order.created,
        };
      });

      return { status: "success", orders: orders };
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.message);
    }
  }
);

export const fetchRestaurantOrders = createAsyncThunk(
  "orders/fetchRestaurantOrders",
  async (
    data: {
      restaurantId: string;
    },
    thunkApi
  ) => {
    try {
      let ordersId = null;
      try {
        const res = await fetch(
          `${APIURL}/api/orders/get-orders-restaurant/${data.restaurantId}`
        );
        const resData = await res.json();
        ordersId = resData;
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }

      let orders: any = null;

      try {
        orders = ordersId.map(async (orderId: any) => {
          const res = await fetch(
            `${APIURL}/api/orders/get-order/${orderId.id}`
          );
          const resData = await res.json();
          return resData;
        });
        orders = await Promise.all(orders);
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }

      let dishesName: {
        name: string;
        quantity: number;
      }[][] = [];

      try {
        let dishesNameRes = orders.map(async (order: any) => {
          let tempDishesRes = [];
          tempDishesRes = order.dish.map(async (id: string) => {
            const res = await fetch(
              `${APIURL}/api/dishes/get-dish-by-id/${id}`
            );
            const resData = await res.json();
            return {
              id: resData.id,
              name: resData.name,
              quantity: order.itemQuantity[id],
            };
          });
          tempDishesRes = await Promise.all(tempDishesRes);
          return tempDishesRes;
        });

        dishesName = await Promise.all(dishesNameRes);
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }

      let userInfo: {
        id: string;
        mobileNumber: string;
        name: string;
      }[] = [];

      try {
        let userInfoRes = orders.map(async (order: any) => {
          const res = await fetch(
            `${APIURL}/api/users/get-user-by-id/${order.user}`
          );
          const resData = await res.json();
          return {
            id: resData.id,
            mobileNumber: resData.mobileNumber,
            name: resData.name || "No Name",
          };
        });
        userInfo = await Promise.all(userInfoRes);
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }

      let restaurantInfo: {
        id: string;
        name: string;
      }[] = [];

      try {
        let restaurantInfoRes = orders.map(async (order: any) => {
          const res = await fetch(
            `${APIURL}/api/restaurants/get-restaurant-by-id/${order.restaurant}`
          );
          const resData = await res.json();
          return {
            id: resData.id,
            name: resData.name,
          };
        });
        restaurantInfo = await Promise.all(restaurantInfoRes);
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }

      orders = orders.map((order: any, index: any) => {
        return {
          id: order.id,
          dishes: dishesName[index],
          restaurant: restaurantInfo[index].name,
          restaurantId: restaurantInfo[index].id,
          userId: userInfo[index].id,
          user: userInfo[index].name,
          mobileNumber: userInfo[index].mobileNumber,
          amount: order.totalPrice,
          status: order.status,
          createdAt: order.created,
        };
      });

      return { status: "success", orders: orders };
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.message);
    }
  }
);

const orderSlice = createSlice({
  name: "orders",
  initialState,
  reducers: {
    resetOrders: (state) => {
      return {
        ...state,
        orders: [],
        status: "idle",
        userOrders: [],
        userOrdersStatus: "idle",
      };
    },
    fetchOrdersFromLocalStorage: (state) => {
      try {
        const serializedState = localStorage.getItem("order");
        if (serializedState === null) {
          return state;
        }
        const parsedState = JSON.parse(serializedState);
        return parsedState;
      } catch (error) {
        return state;
      }
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchOrders.pending, (state) => {
      return {
        ...state,
        status: "loading",
      };
    });
    builder.addCase(fetchOrders.fulfilled, (state, action) => {
      saveToLocalStorage({
        ...state,
        orders: action.payload.orders,
        status: "success",
      });
      return {
        ...state,
        status: "success",
        orders: action.payload.orders,
      };
    });
    builder.addCase(fetchOrders.rejected, (state, action) => {
      return {
        ...state,
        status: "failed",
      };
    });
    builder.addCase(fetchUserOrders.pending, (state) => {
      return {
        ...state,
        userOrdersStatus: "loading",
      };
    });
    builder.addCase(fetchUserOrders.fulfilled, (state, action) => {
      saveToLocalStorage({
        ...state,
        userOrders: action.payload.orders,
        userOrdersStatus: "success",
      });
      return {
        ...state,
        userOrdersStatus: "success",
        userOrders: action.payload.orders,
      };
    });
    builder.addCase(fetchUserOrders.rejected, (state, action) => {
      return {
        ...state,
        userOrdersStatus: "failed",
      };
    });
    builder.addCase(fetchRestaurantOrders.pending, (state) => {
      return {
        ...state,
        restaurantOrdersStatus: "loading",
      };
    });
    builder.addCase(fetchRestaurantOrders.fulfilled, (state, action) => {
      saveToLocalStorage({
        ...state,
        restaurantOrders: action.payload.orders,
        restaurantOrdersStatus: "success",
      });
      return {
        ...state,
        restaurantOrdersStatus: "success",
        restaurantOrders: action.payload.orders,
      };
    });
    builder.addCase(fetchRestaurantOrders.rejected, (state, action) => {
      return {
        ...state,
        restaurantOrdersStatus: "failed",
      };
    });
  },
});

export const { resetOrders, fetchOrdersFromLocalStorage } = orderSlice.actions;
export const orderReducer = orderSlice.reducer;
