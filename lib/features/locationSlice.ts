import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
const APIURL = process.env.NEXT_PUBLIC_API_URL;

interface ILocation {
  id: string;
  stateId: string;
  cityId: string;
  userId: string;
  locality: string;
  houseNumber: string;
  pincode: string;
  isCurrentLocation: boolean;
}

interface IState {
  id: string;
  name: string;
}

interface ICity {
  id: string;
  name: string;
}

interface ILocationInitialState {
  locations: ILocation[];
  status: string;
  states: IState[];
  statesStatus: string;
  cities: {
    stateId: string;
    cities: ICity[];
  }[];
  citiesStatus: string;
}

const saveToLocalStorage = (state: ILocationInitialState) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem("location", serializedState);
  } catch (error) {
    console.log(error);
  }
};

const locationInitialState: ILocationInitialState = {
  locations: [],
  status: "idle",
  states: [],
  cities: [],
  statesStatus: "idle",
  citiesStatus: "idle",
};

export const fetchLocations = createAsyncThunk(
  "fetchLocations",
  async (
    data: {
      userId: string;
    },
    thunkApi
  ) => {
    try {
      let locations = null;
      const res = await fetch(
        `${APIURL}/api/locations/get-all-locations/${data.userId}`
      );
      const resData = await res.json();
      locations = resData.map((location: any) => {
        return {
          id: location.id,
          stateId: location.stateId,
          cityId: location.cityId,
          userId: location.userId,
          locality: location.locality,
          houseNumber: location.houseNumber,
          pincode: location.pincode,
          isCurrentLocation: location.isCurrentLocation,
        };
      });
      return locations;
    } catch (error: any) {
      return thunkApi.rejectWithValue({ error: error.message });
    }
  }
);

export const fetchStates = createAsyncThunk("fetchStates", async () => {
  try {
    let states = null;
    const res = await fetch(`${APIURL}/api/location/get-all-states`);
    const resData = await res.json();
    states = resData.map((state: any) => {
      return {
        id: state.id,
        name: state.name,
      };
    });
    return states;
  } catch (error: any) {
    console.log(error.message);
  }
});

export const fetchCities = createAsyncThunk(
  "fetchCities",
  async (date: {}, thunkApi) => {
    const states = (thunkApi.getState() as any).location.states;

    let cities: {
      stateId: string;
      cities: ICity[];
    }[] = [];

    for (let i = 0; i < states.length; i++) {
      try {
        let citiesData = null;
        const res = await fetch(
          `${APIURL}/api/location/get-all-cities/${states[i].id}`
        );
        const resData = await res.json();
        citiesData = resData.map((city: any) => {
          return {
            id: city.id,
            name: city.name,
          };
        });
        cities.push({
          stateId: states[i].id,
          cities: citiesData,
        });
      } catch (error: any) {
        return thunkApi.rejectWithValue({ error: error.message });
      }
    }

    return cities;
  }
);

const locationSlice = createSlice({
  name: "location",
  initialState: locationInitialState,
  reducers: {
    addLocations: (state, action) => {
      saveToLocalStorage({
        ...state,
        locations: action.payload,
      });
      return {
        ...state,
        locations: action.payload,
      };
    },
    resetLocations: (state) => {
      return {
        ...state,
        locations: [],
        status: "idle",
        cities: [],
        states: [],
      };
    },
    fetchLocationFromLocalStorage: (state) => {
      const location = localStorage.getItem("location");
      if (location) {
        return JSON.parse(location);
      }
      return state;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchLocations.pending, (state) => {
      return {
        ...state,
        status: "loading",
      };
    });
    builder.addCase(fetchLocations.fulfilled, (state, action) => {
      saveToLocalStorage({
        ...state,
        locations: action.payload,
        status: "success",
      });
      return {
        ...state,
        locations: action.payload,
        status: "success",
      };
    });
    builder.addCase(fetchLocations.rejected, (state, action) => {
      return {
        ...state,
        status: "failed",
      };
    });
    builder.addCase(fetchStates.pending, (state) => {
      return {
        ...state,
        statesStatus: "loading",
      };
    });
    builder.addCase(fetchStates.fulfilled, (state, action) => {
      saveToLocalStorage({
        ...state,
        states: action.payload,
        statesStatus: "success",
      });
      return {
        ...state,
        states: action.payload,
        statesStatus: "success",
      };
    });
    builder.addCase(fetchStates.rejected, (state, action) => {
      return {
        ...state,
        statesStatus: "failed",
      };
    });
    builder.addCase(fetchCities.pending, (state) => {
      return {
        ...state,
        citiesStatus: "loading",
      };
    });
    builder.addCase(fetchCities.fulfilled, (state, action) => {
      saveToLocalStorage({
        ...state,
        cities: action.payload,
        citiesStatus: "success",
      });
      console.log(action.payload);
      return {
        ...state,
        cities: action.payload,
        citiesStatus: "success",
      };
    });
    builder.addCase(fetchCities.rejected, (state, action) => {
      return {
        ...state,
        citiesStatus: "failed",
      };
    });
  },
});

export const { addLocations, resetLocations, fetchLocationFromLocalStorage } =
  locationSlice.actions;
export default locationSlice.reducer;
