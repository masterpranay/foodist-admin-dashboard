import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { setLoginRestaurantId } from "./loginSlice";
const APIURL = process.env.NEXT_PUBLIC_API_URL;

interface IRestaurant {
  id: string;
  name: string;
  address: string;
  image: string;
  rating: string;
  categories: string;
  orders?: number;
  city: string;
  state: string;
  pincode: string;
}

interface IRestaurantInitialState {
  restaurant: IRestaurant;
  status: string;
  editStatus?: string;
  restaurants?: IRestaurant[];
  restaurantsStatus?: "idle" | "loading" | "success" | "failed";
  addStatus?: "idle" | "loading" | "success" | "failed";
  deleteStatus?: "idle" | "loading" | "success" | "failed";
}

const saveToLocalStorage = (state: IRestaurantInitialState) => {
  try {
    const serializedState = JSON.stringify({
      restaurant: state.restaurant,
      status: state.status,
      editStatus: state.editStatus,
      restaurants: state.restaurants,
      restaurantsStatus: state.restaurantsStatus,
      addStatus: state.addStatus,
      deleteStatus: state.deleteStatus,
    });
    localStorage.setItem("restaurant", serializedState);
  } catch (error) {
    console.log(error);
  }
};

const restaurantInitialState: IRestaurantInitialState = {
  restaurant: {
    id: "",
    name: "",
    address: "",
    image: "",
    rating: "",
    categories: "",
    city: "",
    state: "",
    pincode: "",
  },
  status: "idle",
  editStatus: "idle",
  restaurants: [],
  restaurantsStatus: "idle",
};

export const fetchAllRestaurants = createAsyncThunk(
  "fetchAllRestaurants",
  async (data, thunkApi) => {
    let restaurantsId = null;
    try {
      const res = await fetch(
        `${APIURL}/api/restaurants/get-all-restaurants-id`
      );
      const resData = await res.json();
      restaurantsId = resData;
    } catch (error: any) {
      console.log(error.message);
      throw new Error(error.message);
    }

    let restaurants = null;
    try {
      restaurants = restaurantsId.map(async (restaurantId: any) => {
        const res = await fetch(
          `${APIURL}/api/restaurants/get-restaurant-by-id/${restaurantId.id}`
        );
        const resData = await res.json();
        return {
          id: resData.id,
          name: resData.name,
          address: resData.address,
          image: resData.image,
          rating: resData.rating,
          categories: resData.categories,
          city: resData.city,
          state: resData.state,
          pincode: resData.pincode,
        };
      });
    } catch (error: any) {
      console.log(error.message);
      throw new Error(error.message);
    }

    restaurants = await Promise.all(restaurants);
    return { status: "success", restaurants: restaurants };
  }
);

export const fetchRestaurant = createAsyncThunk(
  "fetchRestaurant",
  async (
    data: {
      userId?: string;
      restaurantId?: string;
    },
    thunkApi
  ) => {
    try {
      let restaurant = null;
      try {
        if (data.restaurantId) {
          const res = await fetch(
            `${APIURL}/api/restaurants/get-restaurant-by-id/${data.restaurantId}`
          );
          const resData = await res.json();
          restaurant = resData;
        } else if (data.userId) {
          const res = await fetch(
            `${APIURL}/api/restaurants/get-restaurant-by-userId/${data.userId}`
          );
          const resData = await res.json();
          restaurant = resData;
        }
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }
      thunkApi.dispatch(setLoginRestaurantId(restaurant.id));
      thunkApi.dispatch(
        updateRestaurant({
          id: restaurant.id,
          name: restaurant.name,
          address: restaurant.address,
          image: restaurant.image,
          rating: restaurant.rating,
          categories: restaurant.categories,
          city: restaurant.city,
          state: restaurant.state,
          pincode: restaurant.pincode,
        })
      );
      return { status: "success" };
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.message);
    }
  }
);

export const addRestaurant = createAsyncThunk(
  "addRestaurant",
  async (
    data: {
      userId: string;
      name: string;
      address: string;
      image: string;
      rating: string;
      categories: string;
      city: string;
      state: string;
      pincode: string;
    },
    thunkApi
  ) => {
    try {
      let restaurant = null;
      try {
        const res = await fetch(`${APIURL}/api/restaurants/create-restaurant`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            userId: data.userId,
            name: data.name,
            address: data.address,
            image: data.image,
            rating: data.rating,
            categories: data.categories,
            city: data.city,
            state: data.state,
            pincode: data.pincode,
          }),
        });
        const resData = await res.json();
        restaurant = resData;
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }
      console.log(restaurant);
      return { status: "success", restaurant: restaurant };
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.message);
    }
  }
);

export const editRestaurant = createAsyncThunk(
  "editRestaurant",
  async (
    data: {
      restaurant: IRestaurant;
    },
    thunkApi
  ) => {
    try {
      let restaurant = null;
      try {
        const res = await fetch(
          `${APIURL}/api/restaurants/update-restaurant/${data.restaurant.id}`,
          {
            method: "PATCH",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              name: data.restaurant.name,
              address: data.restaurant.address,
              image: data.restaurant.image,
              rating: data.restaurant.rating,
              categories: data.restaurant.categories,
              city: data.restaurant.city,
              state: data.restaurant.state,
              pincode: data.restaurant.pincode,
            }),
          }
        );
        const resData = await res.json();
        restaurant = resData;
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }
      thunkApi.dispatch(
        updateRestaurant({
          id: restaurant.id,
          name: restaurant.name,
          address: restaurant.address,
          image: restaurant.image,
          rating: restaurant.rating,
          categories: restaurant.categories,
          city: restaurant.city,
          state: restaurant.state,
          pincode: restaurant.pincode,
        })
      );
      return { status: "success" };
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.message);
    }
  }
);

export const deleteRestaurant = createAsyncThunk(
  "deleteRestaurant",
  async (
    data: {
      restaurantId: string;
    },
    thunkApi
  ) => {
    try {
      try {
        const res = await fetch(
          `${APIURL}/api/restaurants/delete-restaurant/${data.restaurantId}`,
          {
            method: "DELETE",
          }
        );
        const resData = await res.json();
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }
      return { status: "success" };
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.message);
    }
  }
);

const restaurantSlice = createSlice({
  name: "restaurant",
  initialState: restaurantInitialState,
  reducers: {
    updateRestaurant: (state, action) => {
      saveToLocalStorage({
        ...state,
        restaurant: action.payload,
      });
      return {
        ...state,
        restaurant: action.payload,
      };
    },
    resetRestaurant: (state) => {
      return {
        ...state,
        restaurant: {
          id: "",
          name: "",
          address: "",
          image: "",
          rating: "",
          categories: "",
          city: "",
          state: "",
          pincode: "",
        },
        status: "idle",
        editStatus: "idle",
        restaurants: [],
        restaurantsStatus: "idle",
      };
    },
    fetchRestaurantsFromLocalStorage: (state) => {
      const serializedState = localStorage.getItem("restaurant");
      if (serializedState === null) {
        return undefined;
      }
      const data = JSON.parse(serializedState);
      return {
        ...state,
        restaurant: data.restaurant,
        status: data.status,
        editStatus: data.editStatus,
        restaurants: data.restaurants,
        restaurantsStatus: data.restaurantsStatus,
        addStatus: data.addStatus,
        deleteStatus: data.deleteStatus,
      };
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchRestaurant.pending, (state, action) => {
        return {
          ...state,
          status: "loading",
        };
      })
      .addCase(fetchRestaurant.fulfilled, (state, action) => {
        saveToLocalStorage({
          ...state,
          status: "success",
        });
        return {
          ...state,
          status: "success",
        };
      })
      .addCase(fetchRestaurant.rejected, (state, action) => {
        return {
          ...state,
          status: "failed",
        };
      })
      .addCase(editRestaurant.pending, (state, action) => {
        return {
          ...state,
          editStatus: "loading",
        };
      })
      .addCase(editRestaurant.fulfilled, (state, action) => {
        saveToLocalStorage({
          ...state,
          editStatus: "success",
        });
        return {
          ...state,
          editStatus: "success",
        };
      })
      .addCase(editRestaurant.rejected, (state, action) => {
        return {
          ...state,
          editStatus: "failed",
        };
      })
      .addCase(fetchAllRestaurants.pending, (state, action) => {
        return {
          ...state,
          restaurantsStatus: "loading",
        };
      })
      .addCase(fetchAllRestaurants.fulfilled, (state, action) => {
        saveToLocalStorage({
          ...state,
          restaurants: action.payload.restaurants,
          restaurantsStatus: "success",
        });
        return {
          ...state,
          restaurants: action.payload.restaurants,
          restaurantsStatus: "success",
        };
      })
      .addCase(fetchAllRestaurants.rejected, (state, action) => {
        return {
          ...state,
          restaurantsStatus: "failed",
        };
      })
      .addCase(addRestaurant.pending, (state, action) => {
        return {
          ...state,
          addStatus: "loading",
        };
      })
      .addCase(addRestaurant.fulfilled, (state, action) => {
        saveToLocalStorage({
          ...state,
          restaurants: [
            ...(state.restaurants as IRestaurant[]),
            action.payload.restaurant,
          ],
          addStatus: "success",
        });

        return {
          ...state,
          restaurants: [
            ...(state.restaurants as IRestaurant[]),
            action.payload.restaurant,
          ],
          addStatus: "success",
        };
      })
      .addCase(addRestaurant.rejected, (state, action) => {
        return {
          ...state,
          addStatus: "failed",
        };
      })
      .addCase(deleteRestaurant.pending, (state, action) => {
        return {
          ...state,
          deleteStatus: "loading",
        };
      })
      .addCase(deleteRestaurant.fulfilled, (state, action) => {
        saveToLocalStorage({
          ...state,
          restaurants: state.restaurants?.filter(
            (restaurant) => restaurant.id !== action.meta.arg.restaurantId
          ),
          deleteStatus: "success",
        });

        return {
          ...state,
          restaurants: state.restaurants?.filter(
            (restaurant) => restaurant.id !== action.meta.arg.restaurantId
          ),
          deleteStatus: "success",
        };
      })
      .addCase(deleteRestaurant.rejected, (state, action) => {
        return {
          ...state,
          deleteStatus: "failed",
        };
      });
  },
});

export const {
  updateRestaurant,
  resetRestaurant,
  fetchRestaurantsFromLocalStorage,
} = restaurantSlice.actions;
export const restaurantReducer = restaurantSlice.reducer;
