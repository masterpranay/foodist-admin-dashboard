"use client";

import { Input } from "@/components/ui/input";
import {
  BookA,
  CookingPot,
  Gauge,
  Menu,
  UserRound,
  Utensils,
} from "lucide-react";
import { useEffect, useMemo, useState } from "react";
import { cn } from "@/lib/utils";
import Link from "next/link";
import { Button } from "@/components/ui/button";
import { useRouter } from "next/navigation";
import { useAppDispatch, useAppSelector } from "@/lib/hooks";
import { fetchLoginFromLocalStorage, logout } from "@/lib/features/loginSlice";
import {
  fetchRestaurantsFromLocalStorage,
  resetRestaurant,
} from "@/lib/features/restaurantSlice";
import {
  fetchDishesFromLocalStorage,
  resetDishes,
} from "@/lib/features/dishSlice";
import {
  fetchOrdersFromLocalStorage,
  resetOrders,
} from "@/lib/features/orderSlice";
import {
  fetchUsersFromLocalStorage,
  resetUser,
} from "@/lib/features/usersSlice";
import {
  fetchCities,
  fetchStates,
  resetLocations,
} from "@/lib/features/locationSlice";

const ListItem = ({
  name,
  icon,
  link,
  isActive,
}: {
  name: string;
  icon: any;
  link: string;
  isActive?: boolean;
}) => {
  return (
    <li
      className={cn([
        "flex flex-row items-end gap-2 text-white text-sm",
        isActive && "text-[#01b460]",
      ])}
    >
      {icon}
      <Link href={`/dashboard/${link}`}>
        <span>{name}</span>
      </Link>
    </li>
  );
};

const Sidebar = ({ isSidebarOpen }: { isSidebarOpen: boolean }) => {
  const adminItems = useMemo(
    () => [
      {
        name: "Dashboard",
        icon: <Gauge size="20" />,
        link: "",
      },
      {
        name: "Restaurants",
        icon: <Utensils size="20" />,
        link: "restaurants",
      },
      {
        name: "Users",
        icon: <UserRound size="20" />,
        link: "users",
      },
      {
        name: "Coupon Codes",
        icon: <BookA size="20" />,
        link: "coupons",
      },
    ],
    []
  );

  const restaurantOwnerItems = useMemo(
    () => [
      {
        name: "Dashboard",
        icon: <Gauge size="20" />,
        link: "",
      },
      {
        name: "Dishes",
        icon: <CookingPot size="20" />,
        link: "dishes",
      },
      {
        name: "Past Orders",
        icon: <BookA size="20" />,
        link: "orders",
      },
      {
        name: "Settings",
        icon: <BookA size="20" />,
        link: "settings",
      },
    ],
    []
  );

  const dispatch = useAppDispatch();

  const [userType, setUserType] = useState(
    useAppSelector((state) => state.login.role)
  );

  const [activeItem, setActiveItem] = useState("Dashboard");
  const router = useRouter();

  const title = useMemo(() => {
    if (userType === "admin") {
      return "Admin";
    }

    return "Owner";
  }, [userType]);

  return (
    <aside
      className={cn([
        "text-white transition-all duration-300 ease-in-out w-64 z-50 fixed h-full",
        isSidebarOpen ? "translate-x-0" : "-translate-x-full",
      ])}
    >
      <h1 className="px-8 py-4 border-b-[1px] h-16 flex flex-row gap-2 items-center shadow">
        <span className="text-[#01b460] text-2xl">Foodist</span>
        <span className="bg-[#01b460] text-white px-1 rounded">{title}</span>
      </h1>
      <nav className="px-8 py-8 bg-slate-600 h-full top-16 flex flex-col">
        <span className="text-slate-200">Menu</span>
        <ul className="flex flex-col gap-6 mt-4">
          {userType === "admin" &&
            adminItems.map((item, index) => (
              <ListItem
                key={index}
                name={item.name}
                icon={item.icon}
                link={item.link}
                isActive={item.name === activeItem}
              />
            ))}
          {userType === "owner" &&
            restaurantOwnerItems.map((item, index) => (
              <ListItem
                key={index}
                name={item.name}
                icon={item.icon}
                link={item.link}
                isActive={item.name === activeItem}
              />
            ))}
        </ul>

        <Button
          variant="ghost"
          className="mt-auto mb-12 bg-slate-700 hover:text-white hover:bg-slate-800"
          onClick={async () => {
            // TODO: Logout
            dispatch(logout());
            dispatch(resetRestaurant());
            dispatch(resetDishes());
            dispatch(resetOrders());
            dispatch(resetUser());
            dispatch(resetLocations());
            localStorage.removeItem("login");
            localStorage.removeItem("restaurant");
            localStorage.removeItem("dish");
            localStorage.removeItem("order");
            localStorage.removeItem("user");
            localStorage.removeItem("location");
            router.push("/auth");
          }}
        >
          Logout
        </Button>
      </nav>
    </aside>
  );
};

const Header = ({ setIsSidebarOpen, isSidebarOpen }: any) => {
  return (
    <header
      className={cn([
        "z-50 fixed h-16 flex flex-row items-center bg-white border-b-[1px] border-gray-200 shadow px-8 py-4 transition-all duration-300 ease-in-out",
        isSidebarOpen ? "w-[calc(100%-16rem)]" : "w-full",
      ])}
    >
      <Menu
        className="w-6 h-6 cursor-pointer"
        onClick={() => {
          setIsSidebarOpen((prev: boolean) => !prev);
        }}
      />
      <Input
        className="h-10 ml-4 w-64 bg-slate-50"
        placeholder="Search for something..."
      />
      <UserRound className="ml-auto" />
    </header>
  );
};

export default function DashboardLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const [isSidebarOpen, setIsSidebarOpen] = useState(true);
  const dispatch = useAppDispatch();
  const router = useRouter();
  const loginStatus = useAppSelector((state) => state.login.status);
  const statesStatus = useAppSelector((state) => state.location.statesStatus);

  useEffect(() => {
    dispatch(fetchLoginFromLocalStorage());
    dispatch(fetchRestaurantsFromLocalStorage());
    dispatch(fetchDishesFromLocalStorage());
    dispatch(fetchUsersFromLocalStorage());
    dispatch(fetchOrdersFromLocalStorage());

    dispatch(fetchStates());
  }, []);

  useEffect(() => {
    if (statesStatus === "success") {
      dispatch(fetchCities({}));
    }
  }, [statesStatus]);

  useEffect(() => {
    if (loginStatus != "success") {
      router.push("/auth");
    }
  }, [loginStatus]);

  return (
    <section className="flex flex-row w-full min-h-screen relative">
      <Sidebar isSidebarOpen={isSidebarOpen} />
      <section
        className={cn([
          "flex flex-col w-full h-full transition-all duration-300 ease-in-out",
          isSidebarOpen ? "ml-64" : "ml-0",
        ])}
      >
        <Header
          setIsSidebarOpen={setIsSidebarOpen}
          isSidebarOpen={isSidebarOpen}
        />
        <section className="w-full h-full bg-slate-50 mt-16">
          {children}
        </section>
      </section>
    </section>
  );
}
