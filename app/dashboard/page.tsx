"use client";

import { ColumnDef } from "@tanstack/react-table";
import { BookA, UserIcon, Utensils } from "lucide-react";
import Link from "next/link";
import { DataTable } from "@/components/ui/dataTable";
import { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "@/lib/hooks";
import { fetchRestaurantOrders } from "@/lib/features/orderSlice";
import { toast } from "sonner";

interface Order {
  id: string;
  dishes: {
    id: string;
    name: string;
    quantity: number;
  }[];
  restaurantId: string;
  restaurant: string;
  userId: string;
  user: string;
  mobileNumber: string;
  amount: number;
  status: "pending" | "accepted" | "rejected" | "delivered" | "delivered";
  createdAt: string;
}

const useGetRestaurantsOrders = () => {
  const restaurantId = useAppSelector(
    (state) => state.restaurant.restaurant.id
  );
  const restaurantOrders = useAppSelector(
    (state) => state.order.restaurantOrders
  );
  const loading = useAppSelector((state) => state.order.restaurantOrdersStatus);

  const [pendingOrders, setPendingOrders] = useState<Order[]>([]);
  const [onGoingOrders, setOnGoingOrders] = useState<Order[]>([]);

  const dispatch = useAppDispatch();

  useEffect(() => {
    fetchRestaurantsOrders();
  }, []);

  useEffect(() => {
    if (restaurantOrders) {
      const pending = restaurantOrders.filter(
        (order) => order.status === "pending"
      );
      const onGoing = restaurantOrders.filter(
        (order) => order.status === "accepted"
      );
      setPendingOrders(pending);
      setOnGoingOrders(onGoing);
    }
  }, [restaurantOrders]);

  const fetchRestaurantsOrders = async () => {
    dispatch(
      fetchRestaurantOrders({
        restaurantId: restaurantId,
      })
    );
  };

  return {
    restaurantOrders,
    loading,
    fetchRestaurantsOrders,
    pendingOrders,
    onGoingOrders,
  };
};

const StatsCard = ({
  title,
  value,
  icon,
}: {
  title: string;
  value: string;
  icon: React.ReactNode;
}) => {
  return (
    <div className="flex flex-col items-center justify-center gap-2 p-4 bg-white rounded-md shadow-md">
      <div className="flex items-center justify-center w-12 h-12 bg-slate-100 rounded-full">
        {icon}
      </div>
      <div className="mt-2 text-center">
        <p className="text-sm font-semibold text-gray-600">{title}</p>
        <p className="text-2xl font-bold text-[#01b460] mt-4">{value}</p>
      </div>
    </div>
  );
};

const StatsSection = () => {
  return (
    <div className="grid grid-cols-1 gap-4 md:grid-cols-3">
      <StatsCard title="New Users ( Today )" value="100" icon={<UserIcon />} />
      <StatsCard title="Total Restaurants" value="100" icon={<Utensils />} />
      <StatsCard title="Total Orders ( Today )" value="100" icon={<BookA />} />
    </div>
  );
};

const getOrderColumnDefs = () => {
  const orderColumns: ColumnDef<Order>[] = [
    {
      accessorKey: "dishes",
      header: "Dishes : Quantity",
      cell: ({ row }) => {
        return (
          <div className="flex flex-col gap-2">
            {row.original.dishes.map((dish, index) => (
              <div className="flex items-center gap-2 pb-1" key={index}>
                {dish.name}:<span className="font-bold">{dish.quantity}</span>
              </div>
            ))}
          </div>
        );
      },
    },
    {
      accessorKey: "restaurant",
      header: "Restaurant",
      cell: ({ row }) => {
        return (
          <Link
            href={`/dashboard/restaurant/${row.original.restaurant}`}
            className="text-blue-500 p-1 border-b border-blue-500 cursor-pointer"
          >
            {row.original.restaurant}
          </Link>
        );
      },
    },
    {
      accessorKey: "user",
      header: "User",
      cell: ({ row }) => {
        return (
          <Link
            href={`/dashboard/user/${row.original.user}`}
            className="text-blue-500 p-1 border-b border-blue-500 cursor-pointer"
          >
            {row.original.user}
          </Link>
        );
      },
    },
    {
      accessorKey: "amount",
      header: "Amount",
      cell: ({ row }) => {
        return <span>{row.original.amount}</span>;
      },
    },
    {
      accessorKey: "status",
      header: "Status",
      cell: ({ row }) => {
        return <span>{row.original.status}</span>;
      },
    },
    {
      accessorKey: "createdAt",
      header: "Created At",
      cell: ({ row }) => {
        return <span>{row.original.createdAt}</span>;
      },
    },
  ];

  return orderColumns;
};

const Orders = ({ orders }: { orders: Order[] }) => {
  const columnsOrders = getOrderColumnDefs();

  return (
    <div className="my-10">
      <h2 className="text-2xl font-bold mb-4">New Orders</h2>
      <DataTable columns={columnsOrders} data={orders} />
    </div>
  );
};

const getOrders = (id: string): Order[] => {
  return [];
};

const AdminDashboard = () => {
  return (
    <section className="p-8">
      <h1 className="text-2xl font-bold text-[#01b460] mb-8">Dashboard</h1>
      <StatsSection />
      <Orders orders={getOrders("1")} />
    </section>
  );
};

const PendingOrderCard = ({
  order,
  handleAccept,
  handleReject,
}: {
  order: Order;
  handleAccept: () => void;
  handleReject: () => void;
}) => {
  return (
    <div className="border border-gray-300 p-4 mb-4">
      <h3 className="text-lg font-bold mb-2">Order ID: {order.id}</h3>
      <div>
        <p>Dishes:</p>
        <div className="flex flex-col my-2">
          {order.dishes.map((dish, index) => (
            <div className="flex gap-2 items-center" key={index}>
              <Link
                href={`/dashboard/dishes/${dish.id}`}
                className="pb-1 border-b-2 border-blue-600 text-blue-600"
              >
                {dish.name}
              </Link>
              :<span className="font-bold">{dish.quantity}</span>
            </div>
          ))}
        </div>
      </div>
      <p>User: {order.mobileNumber}</p>
      <p>Amount: {order.amount}</p>
      <p>Status: {order.status}</p>
      <p>Ordered at: {new Date(order.createdAt).toLocaleString()}</p>
      <div className="flex justify-end mt-4">
        <button
          className="bg-green-500 hover:bg-green-600 text-white font-bold py-2 px-4 mr-2 rounded"
          onClick={handleAccept}
        >
          Accept
        </button>
        <button
          className="bg-red-500 hover:bg-red-600 text-white font-bold py-2 px-4 rounded"
          onClick={handleReject}
        >
          Reject
        </button>
      </div>
    </div>
  );
};

const OnGoingOrderCard = ({
  order,
  handleDispatch,
}: {
  order: Order;
  handleDispatch: () => void;
}) => {
  return (
    <div className="border border-gray-300 p-4 mb-4">
      <h3 className="text-lg font-bold mb-2">Order ID: {order.id}</h3>
      <div>
        <p>Dishes:</p>
        <div className="flex flex-col my-2">
          {order.dishes.map((dish, index) => (
            <div className="flex gap-2 items-center" key={index}>
              <Link
                href={`/dashboard/dishes/${dish.id}`}
                className="pb-1 border-b-2 border-blue-600 text-blue-600"
              >
                {dish.name}
              </Link>
              :<span className="font-bold">{dish.quantity}</span>
            </div>
          ))}
        </div>
      </div>
      <p>User: {order.mobileNumber}</p>
      <p>Amount: {order.amount}</p>
      <p>Status: {order.status}</p>
      <p>Ordered at: {new Date(order.createdAt).toLocaleString()}</p>
      <div className="flex justify-end mt-4">
        <button
          className="bg-blue-500 hover:bg-blue-600 text-white font-bold py-2 px-4 rounded"
          onClick={handleDispatch}
        >
          Dispatch
        </button>
      </div>
    </div>
  );
};

const PendingOrdersSection = ({
  pendingOrders,
  setPendingOrders,
  onGoingOrders,
  setOnGoingOrders,
}: {
  pendingOrders: Order[];
  setPendingOrders: (orders: Order[]) => void;
  onGoingOrders: Order[];
  setOnGoingOrders: (orders: Order[]) => void;
}) => {
  const { fetchRestaurantsOrders } = useGetRestaurantsOrders();
  const handleAccept = async (id: string) => {
    try {
      await fetch(
        `${process.env.NEXT_PUBLIC_API_URL}/api/orders/accept-order/${id}`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      toast.success("Order accepted");
      fetchRestaurantsOrders();
    } catch (error: any) {
      console.log(error);
    }
  };

  const handleReject = (id: string) => {
    try {
      fetch(
        `${process.env.NEXT_PUBLIC_API_URL}/api/orders/reject-order/${id}`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      toast.success("Order rejected");
      fetchRestaurantsOrders();
    } catch (error: any) {
      console.log(error);
    }
  };

  return (
    <div className="my-10 p-4 w-1/2">
      <h2 className="text-2xl font-bold mb-4">Pending Orders</h2>
      {pendingOrders.map((order, index) => (
        <PendingOrderCard
          order={order}
          handleAccept={() => handleAccept(order.id)}
          handleReject={() => handleReject(order.id)}
          key={index}
        />
      ))}

      {pendingOrders.length === 0 && (
        <div className="border border-gray-300 p-4 mb-4">
          <p className="text-center">No orders yet</p>
        </div>
      )}
    </div>
  );
};

const OnGoingOrdersSection = ({
  onGoingOrders,
}: {
  onGoingOrders: Order[];
}) => {
  const orders = onGoingOrders;
  const { fetchRestaurantsOrders } = useGetRestaurantsOrders();

  const handleDispatch = (id: string) => {
    try {
      fetch(
        `${process.env.NEXT_PUBLIC_API_URL}/api/orders/dispatch-order/${id}`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      toast.success("Order dispatched");
      fetchRestaurantsOrders();
    } catch (error: any) {
      console.log(error);
    }
  };

  return (
    <div className="my-10 p-4 w-1/2">
      <h2 className="text-2xl font-bold mb-4">On Going Orders</h2>
      {orders.map((order, index) => (
        <OnGoingOrderCard
          order={order}
          key={index}
          handleDispatch={() => handleDispatch(order.id)}
        />
      ))}

      {orders.length === 0 && (
        <div className="border border-gray-300 p-4 mb-4">
          <p className="text-center">No orders yet</p>
        </div>
      )}
    </div>
  );
};

const OwnerDashboard = () => {
  const { pendingOrders, onGoingOrders, loading, fetchRestaurantsOrders } =
    useGetRestaurantsOrders();

  useEffect(() => {
    window.setInterval(() => {
      if (loading != "loading") fetchRestaurantsOrders();
    }, 4000);
  }, []);

  return (
    <section className="p-8">
      <h1 className="text-2xl font-bold text-[#01b460] mb-8 ">Dashboard</h1>
      <div className="flex flex-row gap-4 bg-white p-4 w-full border">
        <PendingOrdersSection
          pendingOrders={pendingOrders}
          setPendingOrders={() => {}}
          onGoingOrders={onGoingOrders}
          setOnGoingOrders={() => {}}
        />
        <OnGoingOrdersSection onGoingOrders={onGoingOrders} />
      </div>
    </section>
  );
};

export default function Dashboard() {
  const userType: string | undefined = useAppSelector(
    (state) => state.login.role
  );

  return (
    <>
      {userType === "admin" && <AdminDashboard />}
      {userType === "owner" && <OwnerDashboard />}
      {userType === "user" && <div>No Access</div>}
    </>
  );
}
