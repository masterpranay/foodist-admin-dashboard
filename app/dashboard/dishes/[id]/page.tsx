"use client";

import { Button } from "@/components/ui/button";
import { DataTable } from "@/components/ui/dataTable";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { deleteDish, updateDish } from "@/lib/features/dishSlice";
import { useAppDispatch, useAppSelector } from "@/lib/hooks";
import { ColumnDef } from "@tanstack/react-table";
import { Loader2, MoreHorizontal } from "lucide-react";
import Link from "next/link";
import { useEffect, useState } from "react";
import Image from "next/image";
import { useRouter } from "next/navigation";
import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { toast } from "sonner";

interface Dish {
  id: string;
  name: string;
  description: string;
  price: number;
  image: string;
  type: "veg" | "non-veg";
  category: string;
}

const TypeSelect = ({
  type,
  handleChange,
  defaultValue,
}: {
  type: "veg" | "non-veg";
  handleChange: any;
  defaultValue?: string;
}) => {
  return (
    <Select defaultValue={defaultValue} name="type">
      <SelectTrigger>
        <SelectValue>{type}</SelectValue>
      </SelectTrigger>
      <SelectContent>
        <SelectGroup>
          <SelectLabel>Types</SelectLabel>
          <SelectItem
            onClick={() => {
              handleChange({ target: { name: "type", value: "veg" } });
            }}
            value="veg"
          >
            veg
          </SelectItem>
          <SelectItem
            onClick={() => {
              handleChange({ target: { name: "type", value: "non-veg" } });
            }}
            value="non-veg"
          >
            non-veg
          </SelectItem>
        </SelectGroup>
      </SelectContent>
    </Select>
  );
};

const DishForm = ({
  dish,
  isEdit = false,
  handleChange,
}: {
  dish?: Dish;
  isEdit?: boolean;
  handleChange: any;
}) => {
  return (
    <div className="grid gap-4 py-4">
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="name" className="text-right">
          Name
        </Label>
        <Input
          id="name"
          className="col-span-3"
          defaultValue={dish?.name}
          name="name"
          onChange={handleChange}
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="description" className="text-right">
          Description
        </Label>
        <Input
          id="description"
          className="col-span-3"
          defaultValue={dish?.description}
          name="description"
          onChange={handleChange}
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="price" className="text-right">
          Price
        </Label>
        <Input
          id="price"
          className="col-span-3"
          defaultValue={dish?.price.toString()}
          name="price"
          onChange={handleChange}
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="image" className="text-right">
          Image
        </Label>
        <Input
          id="image"
          className="col-span-3"
          defaultValue={dish?.image}
          name="image"
          onChange={handleChange}
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="type" className="text-right">
          Type
        </Label>
        <TypeSelect
          type={dish?.type || "veg"}
          handleChange={handleChange}
          defaultValue={dish?.type}
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="category" className="text-right">
          Category
        </Label>
        <Input
          id="category"
          className="col-span-3"
          defaultValue={dish?.category}
          name="category"
          onChange={handleChange}
        />
      </div>
    </div>
  );
};

const EditDish = ({ dish }: { dish: Dish }) => {
  const [dishState, setDishState] = useState<Dish>(dish);
  const dispatch = useAppDispatch();
  const updateDishStatus = useAppSelector((state) => state.dish.updateStatus);
  const deleteDishStatus = useAppSelector((state) => state.dish.deleteStatus);
  const [isDeletionStarted, setIsDeletionStarted] = useState(false);

  const navigate = useRouter();

  const [open, setOpen] = useState(false);
  const [editing, setEditing] = useState(false);

  const handleChange = (e: any) => {
    setDishState({
      ...dishState,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e: any) => {
    e.preventDefault();
    if (updateDishStatus === "loading") return;
    if (
      dishState.name === "" ||
      dishState.description === "" ||
      dishState.price === 0 ||
      dishState.image === "" ||
      dishState.category === ""
    ) {
      toast.error("Please fill all the fields");
      return;
    }
    setEditing(true);
    dispatch(
      updateDish({
        id: dishState.id,
        name: dishState.name,
        description: dishState.description,
        price: dishState.price,
        image: dishState.image,
        type: dishState.type,
        category: dishState.category,
      })
    );
  };

  useEffect(() => {
    if (updateDishStatus === "success" && editing) {
      setOpen(false);
      setEditing(false);
      toast.success("Dish updated successfully");
    }

    if (updateDishStatus === "error" && editing) {
      setEditing(false);
      toast.error("Something went wrong");
    }
  }, [updateDishStatus]);

  useEffect(() => {
    if (isDeletionStarted && deleteDishStatus === "success") {
      navigate.push("/dashboard/dishes");
    }
  }, [deleteDishStatus]);

  return (
    <Dialog open={open}>
      <DialogTrigger asChild>
        <Button
          variant="default"
          className="bg-[#01b460] hover:bg-[#01a256]"
          onClick={() => {
            setOpen(true);
          }}
        >
          <span>Edit Dish</span>
        </Button>
      </DialogTrigger>

      <DialogContent
        className="sm:max-w-[425px]"
        onInteractOutside={() => {
          setOpen(false);
        }}
      >
        <DialogHeader>
          <DialogTitle>Edit Dish</DialogTitle>
          <DialogDescription>
            Fill the Dish Details. Click save when you are done.
          </DialogDescription>
        </DialogHeader>

        <DishForm dish={dishState} isEdit handleChange={handleChange} />

        <DialogFooter className="flex flex-row">
          <Button
            type="button"
            variant="outline"
            className="mr-2"
            onClick={() => {
              setOpen(false);
            }}
          >
            Close
          </Button>
          <Button
            type="submit"
            variant="destructive"
            className=""
            onClick={() => {
              dispatch(
                deleteDish({
                  id: dishState.id,
                })
              );
              setIsDeletionStarted(true);
            }}
            disabled={deleteDishStatus === "loading"}
          >
            {deleteDishStatus === "loading" && (
              <Loader2 className="h-5 w-5 mr-2 animate-spin" />
            )}
            {deleteDishStatus !== "loading" && "Delete"}
          </Button>
          <Button
            type="submit"
            className="bg-[#01b460] hover:bg-[#01a256]"
            onClick={handleSubmit}
            disabled={updateDishStatus === "loading"}
          >
            {updateDishStatus === "loading" && (
              <Loader2 className="h-5 w-5 mr-2 animate-spin" />
            )}
            {updateDishStatus !== "loading" && "Save"}
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

const DishInfo = ({ dish }: { dish: Dish }) => {
  const { name, description, price, image, type, category } = dish;

  return (
    <div className="bg-white rounded-lg shadow-md flex flex-col md:flex-row flex-wrap md:h-96 w-3/4 mt-10">
      <Image
        src={image}
        alt={name}
        width={400}
        height={400}
        className="rounded-l-lg h-full md:w-1/2 min-w-32"
      />
      <div className="p-6 md:w-1/2">
        <h2 className="text-2xl font-bold mb-2">{name}</h2>
        <p className="text-gray-500 mb-2">{description}</p>
        <p className="text-gray-500 mb-2">&#8377;{price}</p>
        <p className="text-gray-500 mb-2">{type}</p>
        <p className="text-gray-500 mb-2">{category}</p>
        <div className="mt-4">
          <EditDish dish={dish} />
        </div>
      </div>
    </div>
  );
};

export default function Dish({
  params,
}: {
  params: {
    id: string;
  };
}) {
  const { id } = params;
  const dishes = useAppSelector((state) => state.dish.dishes);
  const [dish, setDish] = useState<Dish>({
    id,
    name: "",
    description: "",
    price: 0,
    image: "",
    type: "veg",
    category: "",
  });
  const [loading, setLoading] = useState(true);
  const updateDishStatus = useAppSelector((state) => state.dish.updateStatus);

  useEffect(() => {
    setLoading(true);
    const fetchDish = async () => {
      const dishRes = await fetch(
        `${process.env.NEXT_PUBLIC_API_URL}/api/dishes/get-dish-by-id/${id}`
      );
      const dishData = await dishRes.json();
      setDish(dishData);
      setLoading(false);
    };

    if (dishes.length === 0) {
      fetchDish();
    } else {
      setDish(dishes.find((dishItem) => dishItem.id === id) as Dish);
      setLoading(false);
    }
  }, [updateDishStatus, id]);

  return (
    <div className="w-full p-10 bg-slate-50">
      <div className="container mx-auto">
        <div className="flex flex-row justify-between items-center">
          <h1 className="text-3xl font-bold text-[#01b460]">Dish</h1>
          <Link
            href={`/dashboard/dishes`}
            className="text-sm text-gray-500 hover:text-gray-700 pb-1 border-b-2 border-gray-200"
          >
            See all dishes
          </Link>
        </div>

        {!loading && <DishInfo dish={dish} />}
        {loading && <Loader2 className="h-12 w-12 text-[#01b460] mx-auto" />}
      </div>
    </div>
  );
}
