"use client";
import { Button } from "@/components/ui/button";
import { DataTable } from "@/components/ui/dataTable";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DialogClose,
} from "@/components/ui/dialog";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { ScrollArea } from "@/components/ui/scroll-area";
import {
  addDish,
  deleteDish,
  fetchDishes,
  updateDish,
} from "@/lib/features/dishSlice";
import { useAppDispatch, useAppSelector } from "@/lib/hooks";
import { ColumnDef } from "@tanstack/react-table";
import { Loader2, MoreHorizontal } from "lucide-react";
import Link from "next/link";
import { useEffect, useState } from "react";
import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { toast } from "sonner";

interface Dish {
  id: string;
  name: string;
  description: string;
  price: number;
  image: string;
  type: "veg" | "non-veg";
  category: string;
}

const TypeSelect = ({
  type,
  handleChange,
}: {
  type: "veg" | "non-veg";
  handleChange: any;
}) => {
  return (
    <Select>
      <SelectTrigger>
        <SelectValue>{type}</SelectValue>
      </SelectTrigger>
      <SelectContent>
        <SelectGroup>
          <SelectLabel>Types</SelectLabel>
          <SelectItem
            onClick={() => {
              handleChange({ target: { name: "type", value: "veg" } });
            }}
            value="veg"
          >
            veg
          </SelectItem>
          <SelectItem
            onClick={() => {
              handleChange({ target: { name: "type", value: "non-veg" } });
            }}
            value="non-veg"
          >
            non-veg
          </SelectItem>
        </SelectGroup>
      </SelectContent>
    </Select>
  );
};

const DishForm = ({
  dish,
  isEdit = false,
  handleChange,
}: {
  dish?: Dish;
  isEdit?: boolean;
  handleChange: any;
}) => {
  return (
    <ScrollArea className="flex flex-col gap-4 max-h-96">
      <div className="grid grid-cols-4 items-center gap-4 m-4">
        <Label htmlFor="name" className="text-right">
          Name
        </Label>
        <Input
          id="name"
          className="col-span-3"
          defaultValue={dish?.name}
          onChange={handleChange}
          name="name"
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4 m-4">
        <Label htmlFor="description" className="text-right">
          Description
        </Label>
        <Input
          id="description"
          className="col-span-3"
          defaultValue={dish?.description}
          onChange={handleChange}
          name="description"
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4 m-4">
        <Label htmlFor="price" className="text-right">
          Price
        </Label>
        <Input
          id="price"
          className="col-span-3"
          defaultValue={dish?.price.toString()}
          onChange={handleChange}
          name="price"
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4 m-4">
        <Label htmlFor="image" className="text-right">
          Image
        </Label>
        <Input
          id="image"
          className="col-span-3"
          defaultValue={dish?.image}
          onChange={handleChange}
          name="image"
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4 m-4">
        <Label htmlFor="type" className="text-right">
          Type
        </Label>
        <TypeSelect type={dish?.type || "veg"} handleChange={handleChange} />
      </div>
      <div className="grid grid-cols-4 items-center gap-4 m-4">
        <Label htmlFor="category" className="text-right">
          Category
        </Label>
        <Input
          id="category"
          className="col-span-3"
          defaultValue={dish?.category}
          onChange={handleChange}
          name="category"
        />
      </div>
    </ScrollArea>
  );
};

const AddDish = () => {
  const [dish, setDish] = useState<Dish>({
    id: "",
    name: "",
    description: "",
    price: 0,
    image: "",
    type: "veg",
    category: "",
  });

  const dispatch = useAppDispatch();
  const restaurantId = useAppSelector(
    (state) => state.restaurant.restaurant.id
  );
  const addDishStatus = useAppSelector((state) => state.dish.addStatus);

  const [open, setOpen] = useState(false);
  const [adding, setAdding] = useState(false);

  const handleChange = (e: any) => {
    setDish({
      ...dish,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e: any) => {
    e.preventDefault();
    if (
      dish.name === "" ||
      dish.description === "" ||
      dish.price === 0 ||
      dish.image === "" ||
      dish.category === ""
    ) {
      toast.error("Please fill all the fields");
      return;
    }
    setAdding(true);
    dispatch(
      addDish({
        restaurantId: restaurantId,
        name: dish.name,
        description: dish.description,
        price: dish.price,
        image: dish.image,
        type: dish.type,
        category: dish.category,
      })
    );
  };

  useEffect(() => {
    if (addDishStatus === "success") {
      setAdding(false);
      setOpen(false);
      setDish({
        id: "",
        name: "",
        description: "",
        price: 0,
        image: "",
        type: "veg",
        category: "",
      });
    }

    if (addDishStatus === "failed") {
      setAdding(false);
      toast.error("Something went wrong");
    }
  }, [addDishStatus]);

  return (
    <Dialog
      open={open}
    >
      <DialogTrigger asChild>
        <Button variant="default" className="bg-[#01b460] hover:bg-[#01a256]" onClick={() => {
          setOpen(true);
        }}>
          Add Dish
        </Button>
      </DialogTrigger>

      <DialogContent className="sm:max-w-[425px]"
        onInteractOutside={() => {
          setOpen(false);
        }}
      >
        <DialogHeader>
          <DialogTitle>Add Dish</DialogTitle>
          <DialogDescription>
            Fill the Dish Details. Click save when you are done.
          </DialogDescription>
        </DialogHeader>

        <DishForm dish={dish} handleChange={handleChange} />

        <DialogFooter className="flex flex-row">
          <Button
            variant="outline"
            className="mr-2"
            onClick={() => {
              setOpen(false);
            }}
          >
            Close
          </Button>
          <Button
            type="submit"
            className="bg-[#01b460] hover:bg-[#01a256]"
            onClick={handleSubmit}
            disabled={addDishStatus === "loading"}
          >
            {addDishStatus === "loading" && (
              <Loader2 className="h-5 w-5 mr-2 animate-spin" />
            )}
            {addDishStatus !== "loading" && "Add"}
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

const getDishColumnDefs = () => {
  const dishColumns: ColumnDef<Dish>[] = [
    {
      accessorKey: "name",
      header: "Name",
      cell: ({ row }) => {
        return (
          <Link
            href={`/dashboard/dishes/${row.original.id}`}
            className="flex flex-row items-center gap-4"
          >
            <img
              src={row.original.image}
              alt={row.original.name}
              className="h-12 w-12 rounded-lg"
            />
            <h3 className="pb-1 text-blue-600 border-b-2 border-blue-600">
              {row.original.name}
            </h3>
          </Link>
        );
      },
    },
    {
      accessorKey: "description",
      header: "Description",
    },
    {
      accessorKey: "price",
      header: "Price",
    },
    {
      accessorKey: "type",
      header: "Type",
    },
    {
      accessorKey: "category",
      header: "Category",
    },
  ];

  return dishColumns;
};

const DishesTable = ({ dishes }: { dishes: Dish[] }) => {
  return (
    <div className="mb-10">
      <div className="flex flex-row justify-between items-center mb-4">
        <h2 className="text-2xl font-bold">Dishes</h2>
        <AddDish />
      </div>
      <DataTable columns={getDishColumnDefs()} data={dishes} />
    </div>
  );
};

export default function Dishes({}) {
  const restaurantId = useAppSelector(
    (state) => state.restaurant.restaurant.id
  );
  const fetchDishStatus = useAppSelector((state) => state.dish.status);
  const dishesFromState = useAppSelector((state) => state.dish.dishes);
  const [loading, setLoading] = useState(false);
  const [dishes, setDishes] = useState<Dish[]>([]);
  const dispatch = useAppDispatch();

  useEffect(() => {
    setLoading(true);
    if (fetchDishStatus === "idle") {
      dispatch(
        fetchDishes({
          restaurantId,
        })
      );
    }
  }, [fetchDishStatus]);

  useEffect(() => {
    if (fetchDishStatus === "success") {
      setLoading(false);
      setDishes(dishesFromState);
    }
  }, [fetchDishStatus]);

  return (
    <div className="w-full p-10 bg-slate-50">
      <DishesTable dishes={dishes} />
      {loading && (
        <div className="flex flex-row justify-center items-center">
          <Loader2 className="h-10 w-10 animate-spin" />
        </div>
      )}
    </div>
  );
}
