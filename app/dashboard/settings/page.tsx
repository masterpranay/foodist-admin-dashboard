"use client";
import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { editRestaurant } from "@/lib/features/restaurantSlice";
import {
  useAppDispatch,
  useAppSelector,
  useAppThunkDispatch,
} from "@/lib/hooks";
import { Loader2 } from "lucide-react";
import { useEffect, useState } from "react";
import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { fetchStates } from "@/lib/features/locationSlice";

interface Restaurant {
  id: string;
  name: string;
  address: string;
  image: string;
  rating: string;
  categories: string;
  orders?: number;
  city: string;
  state: string;
  pincode: string;
}

interface IState {
  label: string;
  value: string;
}

interface Icity {
  label: string;
  value: string;
}

const StatesSelect = ({
  states,
  selectedValue,
  onChange,
}: {
  states: IState[];
  selectedValue?: string;
  onChange: any;
}) => {
  console.log(selectedValue);
  return (
    <Select defaultValue={selectedValue} onValueChange={onChange} name="state">
      <SelectTrigger className="w-[280px]">
        <SelectValue placeholder="Select a State" />
      </SelectTrigger>
      <SelectContent>
        <SelectGroup>
          <SelectLabel>States</SelectLabel>
          {states.map((state: any, index: number) => {
            return (
              <SelectItem value={state.value} key={index}>
                {state.label}
              </SelectItem>
            );
          })}
        </SelectGroup>
      </SelectContent>
    </Select>
  );
};

const CitiesSelect = ({
  cities,
  selectedValue,
  onChange,
}: {
  cities: Icity[];
  selectedValue?: string;
  onChange: any;
}) => {
  console.log(selectedValue);
  return (
    <Select defaultValue={selectedValue} onValueChange={onChange} name="city">
      <SelectTrigger className="w-[280px]">
        <SelectValue placeholder="Select a City" />
      </SelectTrigger>
      <SelectContent>
        <SelectGroup>
          <SelectLabel>Cities</SelectLabel>
          {cities.map((city: any, index: number) => {
            return (
              <SelectItem value={city.value} key={index}>
                {city.label}
              </SelectItem>
            );
          })}
        </SelectGroup>
      </SelectContent>
    </Select>
  );
};

const RestaurantForm = ({
  restaurant,
  isEdit = false,
  handleChange,
}: {
  restaurant?: Restaurant;
  isEdit?: boolean;
  handleChange: any;
}) => {
  const [states, setStates] = useState<IState[]>([]);
  const [cities, setCities] = useState<Icity[]>([]);

  const [selectedState, setSelectedState] = useState<string>("");
  const [selectedCity, setSelectedCity] = useState<string>("");

  const statesFromState = useAppSelector((state) => state.location.states);
  const citiesFromState = useAppSelector((state) => state.location.cities);

  useEffect(() => {
    if (statesFromState) {
      setStates(
        statesFromState.map((state: any) => {
          return {
            label: state.name,
            value: state.id,
          };
        })
      );
    }
  }, [statesFromState]);

  useEffect(() => {
    if (citiesFromState) {
      setCities(
        citiesFromState
          .find((item: any) => item.stateId === restaurant?.state)
          ?.cities.map((city: any) => {
            return {
              label: city.name,
              value: city.id,
            };
          }) || []
      );
    }
  }, [citiesFromState]);

  return (
    <div className="grid gap-4 py-4">
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="name" className="text-right">
          Name
        </Label>
        <Input
          id="name"
          className="col-span-3"
          defaultValue={restaurant?.name}
          onChange={handleChange}
          name="name"
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="address" className="text-right">
          Address
        </Label>
        <Input
          id="address"
          className="col-span-3"
          defaultValue={restaurant?.address}
          onChange={handleChange}
          name="address"
        />
      </div>

      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="state" className="text-right">
          State
        </Label>
        <StatesSelect
          states={states}
          selectedValue={restaurant?.state}
          onChange={(value: any) => {
            setSelectedState(value);
            handleChange({
              target: {
                name: "state",
                value,
              },
            });
          }}
        />
      </div>

      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="city" className="text-right">
          City
        </Label>
        <CitiesSelect
          cities={cities}
          selectedValue={restaurant?.city}
          onChange={(value: any) => {
            setSelectedCity(value);
            handleChange({
              target: {
                name: "city",
                value,
              },
            });
          }}
        />
      </div>

      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="pincode" className="text-right">
          Pincode
        </Label>
        <Input
          id="pincode"
          className="col-span-3"
          defaultValue={restaurant?.pincode}
          onChange={handleChange}
          name="pincode"
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="image" className="text-right">
          Image
        </Label>
        <Input
          id="image"
          className="col-span-3"
          defaultValue={restaurant?.image}
          onChange={handleChange}
          name="image"
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="rating" className="text-right">
          Rating
        </Label>
        <Input
          id="rating"
          className="col-span-3"
          defaultValue={restaurant?.rating}
          onChange={handleChange}
          name="rating"
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="categories" className="text-right">
          Categories
        </Label>
        <Input
          id="categories"
          className="col-span-3"
          defaultValue={restaurant?.categories}
          onChange={handleChange}
          name="categories"
        />
      </div>
    </div>
  );
};

const EditRestaurant = ({ restaurant }: { restaurant: Restaurant }) => {
  const [restaurantState, setRestaurantState] =
    useState<Restaurant>(restaurant);
  const dispatch = useAppDispatch();
  const dispatchThunk = useAppThunkDispatch();
  const editRestaurantStatus = useAppSelector(
    (state) => state.restaurant.editStatus
  );

  const handleChange = (e: any) => {
    setRestaurantState({
      ...restaurantState,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e: any) => {
    e.preventDefault();
    if (
      !restaurantState.name ||
      !restaurantState.address ||
      !restaurantState.city ||
      !restaurantState.state ||
      !restaurantState.pincode ||
      !restaurantState.image ||
      !restaurantState.rating ||
      !restaurantState.categories
    ) {
      return;
    }
    dispatchThunk(
      editRestaurant({
        restaurant: restaurantState,
      })
    );
  };

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button variant="default" className="bg-[#01b460] hover:bg-[#01a256]">
          Edit Restaurant
        </Button>
      </DialogTrigger>

      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>Edit Restaurant</DialogTitle>
          <DialogDescription>
            Fill the Restaurant Details. Click save when you are done.
          </DialogDescription>
        </DialogHeader>

        <RestaurantForm
          restaurant={restaurant}
          isEdit
          handleChange={handleChange}
        />

        <DialogFooter className="flex flex-row">
          <Button
            type="submit"
            className="bg-[#01b460] hover:bg-[#01a256]"
            onClick={(e) => {
              handleSubmit(e);
            }}
            disabled={editRestaurantStatus === "loading"}
          >
            {editRestaurantStatus === "loading" && (
              <Loader2 className="animate-spin w-5 h-5 mr-2" />
            )}

            {editRestaurantStatus !== "loading" && "Save"}
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

const RestaurantInfo = ({ restaurant }: { restaurant: Restaurant }) => {
  const { id, name, address, image, rating, categories, city, state, pincode } =
    restaurant;

  const [stateName, setStateName] = useState<string>("");
  const [cityName, setCityName] = useState<string>("");

  const statesFromState = useAppSelector((state) => state.location.states);
  const citiesFromState = useAppSelector((state) => state.location.cities);

  useEffect(() => {
    if (statesFromState && statesFromState.length > 0) {
      const stateName = statesFromState.find(
        (state: any) => state.id === restaurant.state
      )?.name;
      if (stateName) {
        setStateName(stateName);
      }
    }
  }, [restaurant.state]);

  useEffect(() => {
    if (citiesFromState && citiesFromState.length > 0) {
      const cityName = citiesFromState
        .find((item: any) => item.stateId === restaurant.state)
        ?.cities.find((city: any) => city.id === restaurant.city)?.name;

      if (cityName) {
        setCityName(cityName);
      }
    }
  }, [restaurant.city]);

  return (
    <div className="bg-white rounded-lg shadow-md flex flex-col md:flex-row flex-wrap md:h-96 md:w-3/4">
      <img
        src={image}
        alt={name}
        className="h-full rounded-l-lg w-full md:w-1/2"
      />
      <div className="p-6 w-full md:w-1/2">
        <h2 className="text-xl font-bold mb-2">{name}</h2>
        <p className="text-gray-500 mb-2">{address}</p>
        <p className="text-gray-500 mb-2">{cityName}</p>
        <p className="text-gray-500 mb-2">{stateName}</p>
        <p className="text-gray-500 mb-2">{pincode}</p>
        <p className="text-gray-500 mb-2">Rating: {rating}</p>
        <div className="flex flex-wrap mt-4">
          {categories.split(", ").map((category) => (
            <span
              key={category}
              className="bg-slate-100 text-gray-600 rounded-full px-2 py-1 text-sm mr-2 mb-2"
            >
              {category}
            </span>
          ))}
        </div>

        <div className="mt-4">
          <EditRestaurant restaurant={restaurant} />
        </div>
      </div>
    </div>
  );
};

export default function Restaurant({}) {
  const restaurant = useAppSelector((state) => state.restaurant.restaurant);
  return (
    <div className="w-full p-10 bg-slate-50">
      <RestaurantInfo restaurant={restaurant} />
    </div>
  );
}
