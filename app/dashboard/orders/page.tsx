"use client";

import { ColumnDef } from "@tanstack/react-table";
import { DataTable } from "@/components/ui/dataTable";
import { Loader2, MoreHorizontal } from "lucide-react";
import { useEffect, useState } from "react";
import Link from "next/link";
import { useAppDispatch, useAppSelector } from "@/lib/hooks";
import { fetchOrders } from "@/lib/features/orderSlice";

type Order = {
  id: string;
  dishes: {
    id: string;
    name: string;
    quantity: number;
  }[];
  restaurant: string;
  user: string;
  mobileNumber: string;
  amount: number;
  status: string;
  createdAt: string;
};

const columns: ColumnDef<Order>[] = [
  {
    accessorKey: "dishes",
    header: "Dishes : Quantity",
    cell: ({ row }) => {
      return (
        <div className="flex flex-col gap-2">
          {row.original.dishes.map((dish, index) => (
            <div className="flex items-center gap-2 pb-1 " key={index}>
              <Link
                href={`/dashboard/dishes/${dish.id}`}
                className="text-blue-500 p-1 border-b border-blue-500 cursor-pointer"
              >
                {dish.name}:
              </Link>
              <span className="font-bold">{dish.quantity}</span>
            </div>
          ))}
        </div>
      );
    },
  },
  {
    accessorKey: "user",
    header: "User",
    cell: ({ row }) => {
      return <div>{row.original.user}</div>;
    },
  },
  {
    accessorKey: "mobileNumber",
    header: "Mobile Number",
    cell: ({ row }) => {
      return <div>{row.original.mobileNumber}</div>;
    },
  },
  {
    accessorKey: "amount",
    header: "Amount",
    cell: ({ row }) => {
      return <span>{row.original.amount}</span>;
    },
  },
  {
    accessorKey: "status",
    header: "Status",
    cell: ({ row }) => {
      return <span>{row.original.status}</span>;
    },
  },
  {
    accessorKey: "createdAt",
    header: "Created At",
    cell: ({ row }) => {
      return <span>{row.original.createdAt}</span>;
    },
  },
];

const Header = () => {
  return (
    <div className="flex justify-between items-center mb-10">
      <h1 className="text-3xl font-bold">Past Orders</h1>
    </div>
  );
};

export default function OrdersPage() {
  const restaurantId = useAppSelector(
    (state) => state.restaurant.restaurant.id
  );
  const orders = useAppSelector((state) => state.order.orders);
  const ordersStatus = useAppSelector((state) => state.order.status);
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (ordersStatus === "idle") {
      if (!restaurantId) return;
      dispatch(
        fetchOrders({
          restaurantId,
        })
      );
    }
  }, [ordersStatus]);

  return (
    <div className="container mx-auto p-10 relative">
      <Header />
      <DataTable columns={columns} data={orders} />
      {ordersStatus == "loading" && <Loader2 className="text-[#01b460] animate-spin absolute top-12 left-1/2" />}
    </div>
  );
}
