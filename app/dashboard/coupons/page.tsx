"use client";

import { useState } from "react";
import { ColumnDef } from "@tanstack/react-table";
import { DataTable } from "@/components/ui/dataTable";
import { MoreHorizontal } from "lucide-react";
import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DialogFooter,
} from "@/components/ui/dialog";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";

type Coupon = {
  id: string;
  code: string;
  amount: string;
};

const CouponForm = ({
  coupon,
  isEdit = false,
}: {
  coupon?: Coupon;
  isEdit?: boolean;
}) => {
  return (
    <div className="grid gap-4 py-4">
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="code" className="text-right">
          Code
        </Label>
        <Input id="code" className="col-span-3" defaultValue={coupon?.code} />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="amount" className="text-right">
          Amount
        </Label>
        <Input
          id="amount"
          className="col-span-3"
          defaultValue={coupon?.amount}
        />
      </div>
    </div>
  );
};

const AddCoupon = () => {
  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button variant="default" className="bg-[#01b460] hover:bg-[#01a256]">
          New Coupon
        </Button>
      </DialogTrigger>

      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>Add Coupon</DialogTitle>
          <DialogDescription>
            Fill in the Coupon Details. Click save when you are done.
          </DialogDescription>
        </DialogHeader>

        <CouponForm />

        <DialogFooter>
          <Button type="submit" className="bg-[#01b460] hover:bg-[#01a256]">
            Add
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

const EditCoupon = ({ coupon }: { coupon: Coupon }) => {
  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button variant="ghost" className="h-8 w-8 p-0">
          <span className="sr-only">Open menu</span>
          <MoreHorizontal className="h-4 w-4" />
        </Button>
      </DialogTrigger>

      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>Edit Coupon</DialogTitle>
          <DialogDescription>
            Fill in the Coupon Details. Click save when you are done.
          </DialogDescription>
        </DialogHeader>

        <CouponForm coupon={coupon} isEdit />

        <DialogFooter className="flex flex-row">
          <Button type="submit" variant="destructive" className="">
            Delete
          </Button>
          <Button type="submit" className="bg-[#01b460] hover:bg-[#01a256]">
            Save
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

const columns: ColumnDef<Coupon>[] = [
  {
    accessorKey: "code",
    header: () => {
      return <div className="text-left">Code</div>;
    },
  },
  {
    accessorKey: "amount",
    header: () => {
      return <div className="text-left">Amount</div>;
    },
  },
  {
    accessorKey: "id",
    header: () => {
      return <div className="text-right">Actions</div>;
    },
    cell: ({ row }) => {
      const restaurant = row.original;

      return (
        <div className="flex flex-row justify-end gap-2">
          <EditCoupon coupon={restaurant} />
        </div>
      );
    },
  },
];

async function getData(): Promise<Coupon[]> {
  return [
    {
      id: "1",
      code: "FIRSTORDER",
      amount: "10%",
    },
    {
      id: "2",
      code: "SECONDORDER",
      amount: "20%",
    },
    {
      id: "3",
      code: "THIRDORDER",
      amount: "30%",
    },
  ];
}

const Header = () => {
  return (
    <div className="flex items-center justify-between mb-10">
      <h1 className="text-2xl font-bold text-[#01b460]">Coupons</h1>

      <AddCoupon />
    </div>
  );
};

export default async function DemoPage() {
  const data = await getData();

  return (
    <div className="container mx-auto p-10">
      <Header />
      <DataTable columns={columns} data={data} />
    </div>
  );
}
