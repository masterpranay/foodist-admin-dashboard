"use client";

import { Button } from "@/components/ui/button";
import { DataTable } from "@/components/ui/dataTable";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { fetchDishes, resetDishes } from "@/lib/features/dishSlice";
import { fetchOrders, resetOrders } from "@/lib/features/orderSlice";
import {
  deleteRestaurant,
  editRestaurant,
  fetchRestaurant,
  resetRestaurant,
} from "@/lib/features/restaurantSlice";
import { useAppDispatch, useAppSelector } from "@/lib/hooks";
import { ColumnDef } from "@tanstack/react-table";
import { Loader2 } from "lucide-react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { fetchCities, fetchStates } from "@/lib/features/locationSlice";
import { toast } from "sonner";

interface Restaurant {
  id: string;
  name: string;
  address: string;
  image: string;
  rating: string;
  categories: string;
  orders?: number;
  city: string;
  state: string;
  pincode: string;
}

interface Dish {
  id: string;
  name: string;
  description: string;
  price: number;
  image: string;
  type: "veg" | "non-veg";
  category: string;
}

interface Order {
  id: string;
  dishes: {
    name: string;
    quantity: number;
  }[];
  restaurant: string;
  userId: string;
  user: string;
  amount: number;
  status: string;
  createdAt: string;
}

interface IState {
  label: string;
  value: string;
}

interface Icity {
  label: string;
  value: string;
}

const StatesSelect = ({
  states,
  selectedValue,
  onChange,
}: {
  states: IState[];
  selectedValue?: string;
  onChange: any;
}) => {
  console.log(selectedValue);
  return (
    <Select defaultValue={selectedValue} onValueChange={onChange} name="state">
      <SelectTrigger className="w-[280px]">
        <SelectValue placeholder="Select a State" />
      </SelectTrigger>
      <SelectContent>
        <SelectGroup>
          <SelectLabel>States</SelectLabel>
          {states.map((state: any, index: number) => {
            return (
              <SelectItem value={state.value} key={index}>
                {state.label}
              </SelectItem>
            );
          })}
        </SelectGroup>
      </SelectContent>
    </Select>
  );
};

const CitiesSelect = ({
  cities,
  selectedValue,
  onChange,
}: {
  cities: Icity[];
  selectedValue?: string;
  onChange: any;
}) => {
  console.log(selectedValue);
  return (
    <Select defaultValue={selectedValue} onValueChange={onChange} name="city">
      <SelectTrigger className="w-[280px]">
        <SelectValue placeholder="Select a City" />
      </SelectTrigger>
      <SelectContent>
        <SelectGroup>
          <SelectLabel>Cities</SelectLabel>
          {cities.map((city: any, index: number) => {
            return (
              <SelectItem value={city.value} key={index}>
                {city.label}
              </SelectItem>
            );
          })}
        </SelectGroup>
      </SelectContent>
    </Select>
  );
};

const RestaurantForm = ({
  restaurant,
  isEdit = false,
  onChange,
}: {
  restaurant?: Restaurant;
  isEdit?: boolean;
  onChange: any;
}) => {
  const [states, setStates] = useState<IState[]>([]);
  const [cities, setCities] = useState<Icity[]>([]);

  const [selectedState, setSelectedState] = useState<string>("");
  const [selectedCity, setSelectedCity] = useState<string>("");

  const statesFromState = useAppSelector((state) => state.location.states);
  const citiesFromState = useAppSelector((state) => state.location.cities);

  const statesStatus = useAppSelector((state) => state.location.statesStatus);
  const citiesStatus = useAppSelector((state) => state.location.citiesStatus);

  const dispatch = useAppDispatch();

  useEffect(() => {
    if (statesStatus === "idle") {
      dispatch(fetchStates());
    }
  }, [statesStatus]);

  useEffect(() => {
    if (statesFromState) {
      setStates(
        statesFromState.map((state: any) => {
          return {
            label: state.name,
            value: state.id,
          };
        })
      );
    }
  }, [statesFromState]);

  useEffect(() => {
    if (selectedState !== "") {
      dispatch(
        fetchCities({
          stateId: selectedState,
        })
      );
    }
  }, [selectedState]);

  useEffect(() => {
    if (citiesFromState) {
      setCities(
        citiesFromState
          .find((item: any) => item.stateId === restaurant?.state)
          ?.cities.map((city: any) => {
            return {
              label: city.name,
              value: city.id,
            };
          }) || []
      );
    }
  }, [citiesFromState]);

  return (
    <div className="grid gap-4 py-4">
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="name" className="text-right">
          Name
        </Label>
        <Input
          id="name"
          className="col-span-3"
          defaultValue={restaurant?.name}
          onChange={onChange}
          name="name"
          value={restaurant?.name}
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="address" className="text-right">
          Address
        </Label>
        <Input
          id="address"
          className="col-span-3"
          defaultValue={restaurant?.address}
          onChange={onChange}
          name="address"
          value={restaurant?.address}
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="state" className="text-right">
          State
        </Label>
        <StatesSelect
          states={states}
          onChange={(value: string) => {
            setSelectedState(value);
            onChange({
              target: {
                name: "state",
                value: value,
              },
            });
          }}
          selectedValue={restaurant?.state}
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="city" className="text-right">
          City
        </Label>
        <CitiesSelect
          cities={cities}
          onChange={(value: string) => {
            setSelectedCity(value);
            onChange({
              target: {
                name: "city",
                value: value,
              },
            });
          }}
          selectedValue={restaurant?.city}
        />
      </div>

      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="pincode" className="text-right">
          Pincode
        </Label>
        <Input
          id="pincode"
          className="col-span-3"
          defaultValue={restaurant?.pincode}
          onChange={onChange}
          name="pincode"
          value={restaurant?.pincode}
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="image" className="text-right">
          Image
        </Label>
        <Input
          id="image"
          className="col-span-3"
          defaultValue={restaurant?.image}
          onChange={onChange}
          name="image"
          value={restaurant?.image}
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="rating" className="text-right">
          Rating
        </Label>
        <Input
          id="rating"
          className="col-span-3"
          defaultValue={restaurant?.rating}
          onChange={onChange}
          name="rating"
          value={restaurant?.rating}
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="categories" className="text-right">
          Categories
        </Label>
        <Input
          id="categories"
          className="col-span-3"
          defaultValue={restaurant?.categories}
          onChange={onChange}
          name="categories"
          value={restaurant?.categories}
        />
      </div>
    </div>
  );
};

const EditRestaurant = ({ restaurant }: { restaurant: Restaurant }) => {
  const [restaurantEdit, setRestaurantEdit] = useState<Restaurant>(restaurant);
  const dispatch = useAppDispatch();
  const deleteStatus = useAppSelector((state) => state.restaurant.deleteStatus);
  const editStatus = useAppSelector((state) => state.restaurant.editStatus);
  const router = useRouter();

  const [open, setOpen] = useState(false);
  const [editing, setEditing] = useState(false);

  console.log(restaurantEdit);

  useEffect(() => {
    if (editStatus === "success" && editing) {
      setEditing(false);
      setOpen(false);
      toast.success("Restaurant edited successfully");
    }

    if (editStatus === "failed" && editing) {
      setEditing(false);
      toast.error("Restaurant edit failed");
    }
  }, [editStatus]);

  const handleChange = (event: any) => {
    const { name, value } = event.target;
    setRestaurantEdit({ ...restaurantEdit, [name]: value });
  };

  const handleSubmit = (event: any) => {
    event.preventDefault();
    if (
      !restaurantEdit.name ||
      !restaurantEdit.address ||
      !restaurantEdit.city ||
      !restaurantEdit.state ||
      !restaurantEdit.pincode ||
      !restaurantEdit.image ||
      !restaurantEdit.rating ||
      !restaurantEdit.categories
    ) {
      toast.error("Please fill all the fields");
      return;
    }
    setEditing(true);
    dispatch(
      editRestaurant({
        restaurant: restaurantEdit,
      })
    );
  };

  const handleDelete = (event: any) => {
    event.preventDefault();
    dispatch(
      deleteRestaurant({
        restaurantId: restaurant.id,
      })
    );
    router.push("/dashboard/restaurants");
  };

  return (
    <Dialog open={open}>
      <DialogTrigger asChild>
        <Button
          variant="default"
          className="bg-[#01b460] hover:bg-[#01a256]"
          onClick={() => {
            setOpen(true);
          }}
        >
          Edit Restaurant
        </Button>
      </DialogTrigger>

      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>Edit Restaurant</DialogTitle>
          <DialogDescription>
            Fill the Restaurant Details. Click save when you are done.
          </DialogDescription>
        </DialogHeader>

        <RestaurantForm
          restaurant={restaurantEdit}
          isEdit
          onChange={handleChange}
        />

        <DialogFooter className="flex flex-row">
          <Button
            type="button"
            variant="outline"
            className="mr-2"
            onClick={() => {
              setOpen(false);
            }}
          >
            Close
          </Button>
          <Button
            type="submit"
            variant="destructive"
            className=""
            onClick={handleDelete}
          >
            {deleteStatus === "loading" ? (
              <Loader2 className="animate-spin" size={16} />
            ) : (
              "Delete"
            )}
          </Button>
          <Button
            type="submit"
            className="bg-[#01b460] hover:bg-[#01a256]"
            onClick={handleSubmit}
          >
            {editStatus === "loading" ? (
              <Loader2 className="animate-spin" size={16} />
            ) : (
              "Save"
            )}
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

const getOrderColumnDefs = () => {
  const orderColumns: ColumnDef<Order>[] = [
    {
      accessorKey: "dishes",
      header: "Dishes : Quantity",
      cell: ({ row }) => {
        return (
          <div className="flex flex-col gap-2">
            {row.original.dishes.map((dish, index) => (
              <div className="flex items-center gap-2 pb-1" key={index}>
                {dish.name}:<span className="font-bold">{dish.quantity}</span>
              </div>
            ))}
          </div>
        );
      },
    },
    {
      accessorKey: "user",
      header: "User",
      cell: ({ row }) => {
        return (
          <Link
            href={`/dashboard/user/${row.original.userId}`}
            className="text-blue-500 p-1 border-b border-blue-500 cursor-pointer"
          >
            {row.original.user}
          </Link>
        );
      },
    },
    {
      accessorKey: "amount",
      header: "Amount",
      cell: ({ row }) => {
        return <span>{row.original.amount}</span>;
      },
    },
    {
      accessorKey: "status",
      header: "Status",
      cell: ({ row }) => {
        return <span>{row.original.status}</span>;
      },
    },
    {
      accessorKey: "createdAt",
      header: "Created At",
      cell: ({ row }) => {
        return <span>{row.original.createdAt}</span>;
      },
    },
  ];

  return orderColumns;
};

const getDishColumnDefs = () => {
  const dishColumns: ColumnDef<Dish>[] = [
    {
      accessorKey: "name",
      header: "Name",
    },
    {
      accessorKey: "description",
      header: "Description",
    },
    {
      accessorKey: "price",
      header: "Price",
    },
    {
      accessorKey: "type",
      header: "Type",
    },
    {
      accessorKey: "category",
      header: "Category",
    },
  ];

  return dishColumns;
};

const RestaurantInfo = ({
  restaurant,
  isLoading,
}: {
  restaurant: Restaurant;
  isLoading: boolean;
}) => {
  const { id, name, address, image, rating, categories, city, state, pincode } =
    restaurant;

  const [stateName, setStateName] = useState<string>("");
  const [cityName, setCityName] = useState<string>("");

  const statesFromState = useAppSelector((state) => state.location.states);
  const citiesFromState = useAppSelector((state) => state.location.cities);

  useEffect(() => {
    if (statesFromState && statesFromState.length > 0) {
      const stateName = statesFromState.find(
        (state: any) => state.id === restaurant.state
      )?.name;
      if (stateName) {
        setStateName(stateName);
      }
    }
  }, [restaurant.state]);

  useEffect(() => {
    if (citiesFromState && citiesFromState.length > 0) {
      const cityName = citiesFromState
        .find((item: any) => item.stateId === restaurant.state)
        ?.cities.find((city: any) => city.id === restaurant.city)?.name;

      if (cityName) {
        setCityName(cityName);
      }
    }
  }, [restaurant.city]);

  return (
    <div className="bg-white rounded-lg shadow-md flex flex-row flex-wrap md:h-96 w-full">
      {!isLoading && (
        <div className="flex flex-col md:flex-row h-full">
          <Image
            src={image}
            alt={name}
            width={100}
            height={100}
            className="rounded-l-lg w-full md:w-1/2 h-auto min-w-32"
            loader={({ src }) => src}
          />
          <div className="p-6 md:w-1/2">
            <h2 className="text-xl font-bold mb-2">{name}</h2>
            <p className="text-gray-500 mb-2">{address}</p>
            <p className="text-gray-500 mb-2">{cityName}</p>
            <p className="text-gray-500 mb-2">{stateName}</p>
            <p className="text-gray-500 mb-2">{pincode}</p>
            <p className="text-gray-500 mb-2">Rating: {rating}</p>
            <div className="flex flex-wrap mt-4">
              {categories.split(",").map((category) => (
                <span
                  key={category}
                  className="bg-slate-100 text-gray-600 rounded-full px-2 py-1 text-sm mr-2 mb-2"
                >
                  {category}
                </span>
              ))}
            </div>

            <div className="mt-4">
              <EditRestaurant restaurant={restaurant} />
            </div>
          </div>
        </div>
      )}

      {isLoading && (
        <Loader2 className="absolute top-1/2 left-1/2 animate-spin" size={32} />
      )}
    </div>
  );
};

const RestaurantDishes = ({
  dishes,
  isLoading,
}: {
  dishes: Dish[];
  isLoading: boolean;
}) => {
  return (
    <div className="my-10 relative">
      <div className="flex flex-row justify-between items-center mb-4">
        <h2 className="text-2xl font-bold">Dishes</h2>
      </div>
      <DataTable columns={getDishColumnDefs()} data={dishes} />
      {isLoading && (
        <Loader2 className="absolute top-1/2 left-1/2 animate-spin" size={32} />
      )}
    </div>
  );
};

const RestaurantOrders = ({
  orders,
  isLoading,
}: {
  orders: Order[];
  isLoading: boolean;
}) => {
  const columnsOrders = getOrderColumnDefs();

  return (
    <div className="my-10 relative">
      <h2 className="text-2xl font-bold mb-4">Orders</h2>
      <DataTable columns={columnsOrders} data={orders} />
      {isLoading && (
        <Loader2 className="absolute top-1/2 left-1/2 animate-spin" size={32} />
      )}
    </div>
  );
};

export default function Restaurant({
  params,
}: {
  params: {
    id: string;
  };
}) {
  const { id } = params;

  const dishes = useAppSelector((state) => state.dish.dishes);
  const dishStatus = useAppSelector((state) => state.dish.status);
  const orders = useAppSelector((state) => state.order.orders);
  const ordersStatus = useAppSelector((state) => state.order.status);

  const restaurant = useAppSelector((state) => state.restaurant.restaurant);
  const restaurantStatus = useAppSelector((state) => state.restaurant.status);

  const dispatch = useAppDispatch();

  useEffect(() => {
    if (restaurantStatus === "idle") {
      dispatch(
        fetchRestaurant({
          restaurantId: id,
        })
      );
    }
  }, [restaurantStatus]);

  useEffect(() => {
    if (dishStatus === "idle") {
      dispatch(
        fetchDishes({
          restaurantId: id,
        })
      );
    }
  }, [dishStatus]);

  useEffect(() => {
    if (ordersStatus == "idle") {
      dispatch(
        fetchOrders({
          restaurantId: id,
        })
      );
    }
  }, [ordersStatus]);

  useEffect(() => {
    return () => {
      dispatch(resetRestaurant());
      dispatch(resetDishes());
      dispatch(resetOrders());
    };
  }, []);

  return (
    <div className="w-full p-10 bg-slate-50">
      <RestaurantInfo
        restaurant={restaurant}
        isLoading={restaurantStatus === "loading"}
      />
      <RestaurantDishes dishes={dishes} isLoading={dishStatus === "loading"} />
      <RestaurantOrders
        orders={orders}
        isLoading={ordersStatus === "loading"}
      />
    </div>
  );
}
