"use client";

import { ColumnDef } from "@tanstack/react-table";
import { DataTable } from "@/components/ui/dataTable";
import { Loader2 } from "lucide-react";
import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DialogFooter,
  DialogClose,
} from "@/components/ui/dialog";
import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { use, useEffect, useState } from "react";
import Link from "next/link";
import { useAppDispatch, useAppSelector } from "@/lib/hooks";
import {
  addRestaurant,
  fetchAllRestaurants,
} from "@/lib/features/restaurantSlice";
import { toast } from "sonner";
interface Restaurant {
  id: string;
  name: string;
  address: string;
  image: string;
  rating: string;
  categories: string;
  orders?: number;
  city: string;
  state: string;
  pincode: string;
}

interface IState {
  label: string;
  value: string;
}

interface Icity {
  label: string;
  value: string;
}

const StatesSelect = ({
  states,
  selectedValue,
  onChange,
}: {
  states: IState[];
  selectedValue?: string;
  onChange: any;
}) => {
  return (
    <Select
      defaultValue={selectedValue ? selectedValue : ""}
      onValueChange={onChange}
      name="state"
    >
      <SelectTrigger className="w-[280px]">
        <SelectValue placeholder="Select a State" />
      </SelectTrigger>
      <SelectContent>
        <SelectGroup>
          <SelectLabel>States</SelectLabel>
          {states.map((state: any, index: number) => {
            return (
              <SelectItem value={state.value} key={index}>
                {state.label}
              </SelectItem>
            );
          })}
        </SelectGroup>
      </SelectContent>
    </Select>
  );
};

const CitiesSelect = ({
  cities,
  selectedValue,
  onChange,
}: {
  cities: Icity[];
  selectedValue?: string;
  onChange: any;
}) => {
  return (
    <Select
      defaultValue={selectedValue ? selectedValue : ""}
      onValueChange={onChange}
      name="city"
    >
      <SelectTrigger className="w-[280px]">
        <SelectValue placeholder="Select a City" />
      </SelectTrigger>
      <SelectContent>
        <SelectGroup>
          <SelectLabel>Cities</SelectLabel>
          {cities.map((city: any, index: number) => {
            return (
              <SelectItem value={city.value} key={index}>
                {city.label}
              </SelectItem>
            );
          })}
        </SelectGroup>
      </SelectContent>
    </Select>
  );
};

const RestaurantForm = ({
  restaurant,
  isEdit = false,
  handleChange,
}: {
  restaurant?: Restaurant;
  isEdit?: boolean;
  handleChange: any;
}) => {
  const [states, setStates] = useState<IState[]>([]);
  const [cities, setCities] = useState<
    {
      stateId: string;
      cities: Icity[];
    }[]
  >([]);

  const [selectedState, setSelectedState] = useState<string>("");
  const [selectedCity, setSelectedCity] = useState<string>("");

  const statesFromState = useAppSelector((state) => state.location.states);
  const citiesFromState = useAppSelector((state) => state.location.cities);

  const statesStatus = useAppSelector((state) => state.location.statesStatus);
  const citiesStatus = useAppSelector((state) => state.location.citiesStatus);

  const dispatch = useAppDispatch();

  useEffect(() => {
    if (statesFromState) {
      setStates(
        statesFromState.map((state: any) => {
          return {
            label: state.name,
            value: state.id,
          };
        })
      );
    }
  }, [statesFromState]);

  useEffect(() => {
    if (citiesFromState) {
      setCities(
        citiesFromState.map((item: any) => {
          return {
            stateId: item.stateId,
            cities: item.cities.map((city: any) => {
              return {
                label: city.name,
                value: city.id,
              };
            }),
          };
        })
      );
    }
  }, [citiesFromState]);

  return (
    <div className="grid gap-4 py-4">
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="name" className="text-right">
          Name
        </Label>
        <Input
          id="name"
          className="col-span-3"
          defaultValue={restaurant?.name}
          value={restaurant?.name}
          onChange={handleChange}
          name="name"
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="image" className="text-right">
          Image
        </Label>
        <Input
          id="image"
          className="col-span-3"
          defaultValue={restaurant?.image}
          value={restaurant?.image}
          onChange={handleChange}
          name="image"
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="address" className="text-right">
          Address
        </Label>
        <Input
          id="address"
          className="col-span-3"
          defaultValue={restaurant?.address}
          value={restaurant?.address}
          onChange={handleChange}
          name="address"
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="state" className="text-right">
          State
        </Label>
        <StatesSelect
          states={states}
          selectedValue={restaurant?.state}
          onChange={(value: string) => {
            setSelectedState(value);
            handleChange({
              target: {
                name: "state",
                value: value,
              },
            });
          }}
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="city" className="text-right">
          City
        </Label>
        <CitiesSelect
          cities={
            cities.find((item) => item.stateId === selectedState)?.cities || []
          }
          selectedValue={restaurant?.city}
          onChange={(value: string) => {
            setSelectedCity(value);
            handleChange({
              target: {
                name: "city",
                value: value,
              },
            });
          }}
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="pincode" className="text-right">
          Pincode
        </Label>
        <Input
          id="pincode"
          className="col-span-3"
          defaultValue={restaurant?.pincode}
          value={restaurant?.pincode}
          onChange={handleChange}
          name="pincode"
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="rating" className="text-right">
          Rating
        </Label>
        <Input
          id="rating"
          className="col-span-3"
          defaultValue={restaurant?.rating}
          value={restaurant?.rating}
          onChange={handleChange}
          name="rating"
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="categories" className="text-right">
          Categories
        </Label>
        <Input
          id="categories"
          className="col-span-3"
          defaultValue={restaurant?.categories}
          value={restaurant?.categories}
          onChange={handleChange}
          name="categories"
        />
      </div>
    </div>
  );
};

const AddRestaurant = () => {
  const [restaurant, setRestaurant] = useState<Restaurant>({
    id: "",
    name: "",
    address: "",
    image: "",
    rating: "",
    categories: "",
    city: "",
    state: "",
    pincode: "",
  });
  const dispatch = useAppDispatch();
  const userId = useAppSelector((state) => state.login.id);
  const addStatus = useAppSelector((state) => state.restaurant.addStatus);
  const [adding, setadding] = useState(false);

  const [open, setOpen] = useState(false);

  const handleChange = (e: any) => {
    setRestaurant({
      ...restaurant,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e: any) => {
    // e.preventDefault();
    if (
      restaurant.name === "" ||
      restaurant.address === "" ||
      restaurant.image === "" ||
      restaurant.rating === "" ||
      restaurant.categories === "" ||
      restaurant.city === "" ||
      restaurant.state === "" ||
      restaurant.pincode === ""
    ) {
      toast.error("Please fill all the fields");
      return;
    }
    setadding(true);
    dispatch(
      addRestaurant({
        userId,
        name: restaurant.name,
        address: restaurant.address,
        image: restaurant.image,
        rating: restaurant.rating,
        categories: restaurant.categories,
        city: restaurant.city,
        state: restaurant.state,
        pincode: restaurant.pincode,
      })
    );
  };

  useEffect(() => {
    if (addStatus == "success" && adding) {
      setOpen(false);
      setadding(false);
      setRestaurant({
        id: "",
        name: "",
        address: "",
        image: "",
        rating: "",
        categories: "",
        city: "",
        state: "",
        pincode: "",
      });
      toast.success("Restaurant Added Successfully");
    }

    if (addStatus == "failed") {
      setadding(false);
      toast.error("Something went wrong");
    }
  }, [addStatus]);

  return (
    <Dialog open={open}>
      <DialogTrigger asChild>
        <Button
          variant="default"
          className="bg-[#01b460] hover:bg-[#01a256]"
          onClick={() => {
            setOpen(true);
          }}
        >
          New Restaurant
        </Button>
      </DialogTrigger>

      <DialogContent
        className="sm:max-w-[425px]"
        onInteractOutside={() => {
          setOpen(false);
        }}
      >
        <DialogClose
          onClick={() => {
            setOpen(false);
          }}
        />
        <DialogHeader>
          <DialogTitle>Add Restaurant</DialogTitle>
          <DialogDescription>
            Fill the Restaurant Details. Click save when you are done.
          </DialogDescription>
        </DialogHeader>

        <RestaurantForm handleChange={handleChange} />

        <DialogFooter>
          <Button
            variant="destructive"
            onClick={() => {
              setOpen(false);
            }}
          >
            Close
          </Button>

          <Button
            type="submit"
            className="bg-[#01b460] hover:bg-[#01a256]"
            onClick={handleSubmit}
          >
            {addStatus === "loading" ? (
              <Loader2 className="w-6 h-6 animate-spin" />
            ) : (
              <span>Save</span>
            )}
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

const columns: ColumnDef<Restaurant>[] = [
  {
    accessorKey: "image",
    header: () => {
      return <span>Image</span>;
    },
    cell: ({ row }) => {
      return (
        <div className="flex items-center">
          <img
            src={row.original.image}
            alt={row.original.name}
            className="w-10 h-10 rounded-lg mr-4"
          />
        </div>
      );
    },
  },
  {
    accessorKey: "name",
    header: () => {
      return <span>Name</span>;
    },
    cell: ({ row }) => {
      return (
        <Link
          href={`/dashboard/restaurants/${row.original.id}`}
          className="text-blue-600 py-1 border-b border-blue-600"
        >
          {row.original.name}
        </Link>
      );
    },
  },
  {
    accessorKey: "address",
    header: () => {
      return <span>Address</span>;
    },
  },
  {
    accessorKey: "city",
    header: () => {
      return <span>City</span>;
    },
  },
  {
    accessorKey: "state",
    header: () => {
      return <span>State</span>;
    },
  },
  {
    accessorKey: "pincode",
    header: () => {
      return <span>Pincode</span>;
    },
  },
  {
    accessorKey: "rating",
    header: () => {
      return <span>Rating</span>;
    },
  },
  {
    accessorKey: "categories",
    header: () => {
      return <span>Categories</span>;
    },
    cell: ({ row }) => {
      const categories = row.original.categories;

      return <span>{categories}</span>;
    },
  },
];

const Header = () => {
  return (
    <div className="flex items-center justify-between mb-10">
      <h1 className="text-2xl font-bold text-[#01b460]">Restaurants</h1>

      <AddRestaurant />
    </div>
  );
};

export default function RestaurantPage() {
  const restaurantsStatus = useAppSelector(
    (state) => state.restaurant.restaurantsStatus
  );
  const [restaurants, setRestaurants] = useState<Restaurant[]>([]);
  const restaurantsFromState =
    useAppSelector((state) => state.restaurant.restaurants) || [];

  const states = useAppSelector((state) => state.location.states);
  const cities = useAppSelector((state) => state.location.cities);
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (restaurantsStatus === "idle") {
      dispatch(fetchAllRestaurants());
    }
  }, [restaurantsStatus]);

  useEffect(() => {
    if (restaurantsFromState) {
      setRestaurants(
        restaurantsFromState.map((restaurant: any) => {
          return {
            id: restaurant.id,
            name: restaurant.name,
            address: restaurant.address,
            image: restaurant.image,
            rating: restaurant.rating,
            categories: restaurant.categories,
            city:
              cities
                .find((item: any) => item.stateId === restaurant.state)
                ?.cities.find((item) => item.id === restaurant.city)?.name ||
              "",
            state:
              states.find((state) => state.id === restaurant.state)?.name || "",
            pincode: restaurant.pincode,
          };
        })
      );
    }
  }, [restaurantsFromState]);

  return (
    <div className="container mx-auto p-10 relative">
      <Header />
      <DataTable columns={columns} data={restaurants} />
      {restaurantsStatus === "loading" && (
        <Loader2 className="absolute top-1/2 left-1/2 animate-spin w-10 h-10" />
      )}
    </div>
  );
}
