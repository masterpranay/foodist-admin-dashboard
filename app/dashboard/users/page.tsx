"use client";

import { ColumnDef } from "@tanstack/react-table";
import { DataTable } from "@/components/ui/dataTable";
import { useEffect } from "react";
import Link from "next/link";
import { useAppDispatch, useAppSelector } from "@/lib/hooks";
import { fetchAllUsers } from "@/lib/features/usersSlice";
import { Loader2 } from "lucide-react";

interface IUser {
  id: string;
  name: string;
  mobileNumber: string;
  address: string;
}

const columns: ColumnDef<IUser>[] = [
  {
    accessorKey: "name",
    header: "Name",
    cell: ({ row }) => {
      return (
        <Link
          href={`/dashboard/users/${row.original.id}`}
          className="text-blue-600 py-1 border-b border-blue-600"
        >
          {row.original.name ? (
            row.original.name
          ) : (
            <span className="text-gray-400">No Name</span>
          )}
        </Link>
      );
    },
  },
  {
    accessorKey: "mobileNumber",
    header: "Mobile",
  },
  {
    accessorKey: "address",
    header: "Address",
  },
];

const Header = () => {
  return (
    <div className="flex justify-between items-center mb-10">
      <div className="flex-grow">
        <h1 className="text-3xl font-bold text-[#01b460]">Users</h1>
      </div>
    </div>
  );
};

export default function UsersPage() {
  const users = useAppSelector((state) => state.user.users) || [];
  const usersStatus = useAppSelector((state) => state.user.usersStatus);
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (usersStatus === "idle") {
      dispatch(fetchAllUsers());
    }
  }, [usersStatus]);

  return (
    <div className="container mx-auto p-10">
      <Header />
      <DataTable columns={columns} data={users} />
      {usersStatus === "loading" && (
        <Loader2 className="w-10 h-10 text-[#01b460] animate-spin" />
      )}
    </div>
  );
}
