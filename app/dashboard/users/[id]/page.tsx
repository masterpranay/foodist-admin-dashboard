"use client";

import { DataTable } from "@/components/ui/dataTable";
import { fetchUserOrders, resetOrders } from "@/lib/features/orderSlice";
import { useAppDispatch, useAppSelector } from "@/lib/hooks";
import { ColumnDef } from "@tanstack/react-table";
import { Loader2 } from "lucide-react";
import Link from "next/link";
import { useEffect } from "react";

interface IUser {
  id: string;
  name: string;
  mobileNumber: string;
  address: string;
}

interface IOrder {
  id: string;
  dishes: {
    id: string;
    name: string;
    quantity: number;
  }[];
  restaurant: string;
  restaurantId: string;
  userId: string;
  user: string;
  mobileNumber: string;
  amount: number;
  status: string;
  createdAt: string;
}

const getOrderColumnDefs = () => {
  const orderColumns: ColumnDef<IOrder>[] = [
    {
      accessorKey: "dishes",
      header: "Dishes : Quantity",
      cell: ({ row }) => {
        return (
          <div className="flex flex-col gap-2">
            {row.original.dishes.map((dish, index) => (
              <div className="flex items-center gap-2 pb-1" key={index}>
                {dish.name}:<span className="font-bold">{dish.quantity}</span>
              </div>
            ))}
          </div>
        );
      },
    },
    {
      accessorKey: "restaurant",
      header: "Restaurant",
      cell: ({ row }) => {
        return (
          <Link
            href={`/dashboard/restaurants/${row.original.restaurantId}`}
            className="text-blue-500 p-1 border-b border-blue-500 cursor-pointer"
          >
            {row.original.restaurant}
          </Link>
        );
      },
    },
    {
      accessorKey: "amount",
      header: "Amount",
      cell: ({ row }) => {
        return <span>{row.original.amount}</span>;
      },
    },
    {
      accessorKey: "status",
      header: "Status",
      cell: ({ row }) => {
        return <span>{row.original.status}</span>;
      },
    },
    {
      accessorKey: "createdAt",
      header: "Created At",
      cell: ({ row }) => {
        return <span>{row.original.createdAt}</span>;
      },
    },
  ];

  return orderColumns;
};

const UserInfo = ({ user }: { user: IUser }) => {
  const { id, name, address, mobileNumber } = user;

  return (
    <div className="bg-white rounded-lg shadow-md flex flex-row flex-wrap w-3/4">
      <div className="p-6 min-w-64">
        <h2 className="text-xl font-bold mb-2">{name}</h2>
        <p className="text-gray-500 mb-2">{address}</p>
        <p className="text-gray-500 mb-2">Mobile: {mobileNumber}</p>
      </div>
    </div>
  );
};

const UserOrders = ({
  orders,
  isLoading,
}: {
  orders: IOrder[];
  isLoading: boolean;
}) => {
  const columnsOrders = getOrderColumnDefs();
  return (
    <div className="my-10 relative">
      <h2 className="text-2xl font-bold mb-4">Orders</h2>
      <DataTable columns={columnsOrders} data={orders} />
      {isLoading && <Loader2 className="text-green-500 animate-spin absolute top-1/2 left-1/2" size={32}/>}
    </div>
  );
};

export default function Restaurant({
  params,
}: {
  params: {
    id: string;
  };
}) {
  const { id } = params;
  const users = useAppSelector((state) => state.user.users) as IUser[];
  const user = users.find((user) => user.id === id) as IUser;
  const orders = useAppSelector((state) => state.order.userOrders) || [];
  const userOrdersStatus = useAppSelector(
    (state) => state.order.userOrdersStatus
  );
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (userOrdersStatus === "idle") {
      dispatch(
        fetchUserOrders({
          userId: id,
        })
      );
    }
  }, [userOrdersStatus]);

  useEffect(() => {
    return () => {
      dispatch(resetOrders());
    };
  }, []);

  return (
    <div className="w-full p-10 bg-slate-50">
      <UserInfo user={user} />
      <UserOrders orders={orders} isLoading={userOrdersStatus === "loading"} />
    </div>
  );
}
