"use client";

import { Button } from "@/components/ui/button";
import { DataTable } from "@/components/ui/dataTable";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { ColumnDef } from "@tanstack/react-table";
import { MoreHorizontal } from "lucide-react";
import Link from "next/link";

interface Restaurant {
  id: string;
  name: string;
  address: string;
  image: string;
  rating: string;
  categories: string[];
  orders?: number;
  city: string;
  state: string;
  pincode: string;
}

interface Dish {
  id: string;
  name: string;
  description: string;
  price: number;
  image: string;
  type: "veg" | "non-veg";
  category: string;
}

interface Order {
  id: string;
  dishes: {
    name: string;
    quantity: number;
  }[];
  restaurant: string;
  user: string;
  amount: number;
  status: string;
  createdAt: string;
}

const DishForm = ({
  dish,
  isEdit = false,
}: {
  dish?: Dish;
  isEdit?: boolean;
}) => {
  return (
    <div className="grid gap-4 py-4">
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="name" className="text-right">
          Name
        </Label>
        <Input id="name" className="col-span-3" defaultValue={dish?.name} />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="description" className="text-right">
          Description
        </Label>
        <Input
          id="description"
          className="col-span-3"
          defaultValue={dish?.description}
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="price" className="text-right">
          Price
        </Label>
        <Input
          id="price"
          className="col-span-3"
          defaultValue={dish?.price.toString()}
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="image" className="text-right">
          Image
        </Label>
        <Input id="image" className="col-span-3" defaultValue={dish?.image} />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="type" className="text-right">
          Type
        </Label>
        <Input id="type" className="col-span-3" defaultValue={dish?.type} />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="category" className="text-right">
          Category
        </Label>
        <Input
          id="category"
          className="col-span-3"
          defaultValue={dish?.category}
        />
      </div>
    </div>
  );
};

const RestaurantForm = ({
  restaurant,
  isEdit = false,
}: {
  restaurant?: Restaurant;
  isEdit?: boolean;
}) => {
  return (
    <div className="grid gap-4 py-4">
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="name" className="text-right">
          Name
        </Label>
        <Input
          id="name"
          className="col-span-3"
          defaultValue={restaurant?.name}
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="address" className="text-right">
          Address
        </Label>
        <Input
          id="address"
          className="col-span-3"
          defaultValue={restaurant?.address}
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="city" className="text-right">
          City
        </Label>
        <Input id="city" className="col-span-3" defaultValue={restaurant?.city} />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="state" className="text-right">
          State
        </Label>
        <Input
          id="state"
          className="col-span-3"
          defaultValue={restaurant?.state}
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="pincode" className="text-right">
          Pincode
        </Label>
        <Input
          id="pincode"
          className="col-span-3"
          defaultValue={restaurant?.pincode}
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="image" className="text-right">
          Image
        </Label>
        <Input
          id="image"
          className="col-span-3"
          defaultValue={restaurant?.image}
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="rating" className="text-right">
          Rating
        </Label>
        <Input
          id="rating"
          className="col-span-3"
          defaultValue={restaurant?.rating}
        />
      </div>
      <div className="grid grid-cols-4 items-center gap-4">
        <Label htmlFor="categories" className="text-right">
          Categories
        </Label>
        <Input
          id="categories"
          className="col-span-3"
          defaultValue={restaurant?.categories.join(", ")}
        />
      </div>
    </div>
  );
};

const AddDish = () => {
  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button variant="default" className="bg-[#01b460] hover:bg-[#01a256]">
          Add Dish
        </Button>
      </DialogTrigger>

      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>Add Dish</DialogTitle>
          <DialogDescription>
            Fill the Dish Details. Click save when you are done.
          </DialogDescription>
        </DialogHeader>

        <DishForm />

        <DialogFooter className="flex flex-row">
          <Button type="submit" className="bg-[#01b460] hover:bg-[#01a256]">
            Save
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

const EditDish = ({ dish }: { dish: Dish }) => {
  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button variant="ghost" className="h-8 w-8 p-0">
          <span className="sr-only">Open menu</span>
          <MoreHorizontal className="h-4 w-4" />
        </Button>
      </DialogTrigger>

      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>Edit Dish</DialogTitle>
          <DialogDescription>
            Fill the Dish Details. Click save when you are done.
          </DialogDescription>
        </DialogHeader>

        <DishForm dish={dish} isEdit />

        <DialogFooter className="flex flex-row">
          <Button type="submit" variant="destructive" className="">
            Delete
          </Button>
          <Button type="submit" className="bg-[#01b460] hover:bg-[#01a256]">
            Save
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

const EditRestaurant = ({ restaurant }: { restaurant: Restaurant }) => {
  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button variant="default" className="bg-[#01b460] hover:bg-[#01a256]">
          Edit Restaurant
        </Button>
      </DialogTrigger>

      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>Edit Restaurant</DialogTitle>
          <DialogDescription>
            Fill the Restaurant Details. Click save when you are done.
          </DialogDescription>
        </DialogHeader>

        <RestaurantForm restaurant={restaurant} isEdit />

        <DialogFooter className="flex flex-row">
          <Button type="submit" variant="destructive" className="">
            Delete
          </Button>
          <Button type="submit" className="bg-[#01b460] hover:bg-[#01a256]">
            Save
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

const getOrderColumnDefs = () => {
  const orderColumns: ColumnDef<Order>[] = [
    {
      accessorKey: "dishes",
      header: "Dishes : Quantity",
      cell: ({ row }) => {
        return (
          <div className="flex flex-col gap-2">
            {row.original.dishes.map((dish, index) => (
              <div className="flex items-center gap-2 pb-1" key={index}>
                {dish.name}:<span className="font-bold">{dish.quantity}</span>
              </div>
            ))}
          </div>
        );
      },
    },
    {
      accessorKey: "user",
      header: "User",
      cell: ({ row }) => {
        return (
          <Link
            href={`/dashboard/user/${row.original.user}`}
            className="text-blue-500 p-1 border-b border-blue-500 cursor-pointer"
          >
            {row.original.user}
          </Link>
        );
      },
    },
    {
      accessorKey: "amount",
      header: "Amount",
      cell: ({ row }) => {
        return <span>{row.original.amount}</span>;
      },
    },
    {
      accessorKey: "status",
      header: "Status",
      cell: ({ row }) => {
        return <span>{row.original.status}</span>;
      },
    },
    {
      accessorKey: "createdAt",
      header: "Created At",
      cell: ({ row }) => {
        return <span>{row.original.createdAt}</span>;
      },
    },
  ];

  return orderColumns;
};

const getDishColumnDefs = () => {
  const dishColumns: ColumnDef<Dish>[] = [
    {
      accessorKey: "name",
      header: "Name",
    },
    {
      accessorKey: "description",
      header: "Description",
    },
    {
      accessorKey: "price",
      header: "Price",
    },
    {
      accessorKey: "type",
      header: "Type",
    },
    {
      accessorKey: "category",
      header: "Category",
    },
    {
      accessorKey: "actions",
      header: "",
      cell: ({ row }) => {
        return <EditDish dish={row.original} />;
      },
    },
  ];

  return dishColumns;
};

const RestaurantInfo = ({ restaurant }: { restaurant: Restaurant }) => {
  const { id, name, address, image, rating, categories, city, state, pincode } = restaurant;

  return (
    <div className="bg-white rounded-lg shadow-md flex flex-row flex-wrap h-96 w-3/4">
      <img src={image} alt={name} className="h-full w-auto rounded-l-lg" />
      <div className="p-6 min-w-64">
        <h2 className="text-xl font-bold mb-2">{name}</h2>
        <p className="text-gray-500 mb-2">{address}</p>
        <p className="text-gray-500 mb-2">{city}</p>
        <p className="text-gray-500 mb-2">{state}</p>
        <p className="text-gray-500 mb-2">{pincode}</p>
        <p className="text-gray-500 mb-2">Rating: {rating}</p>
        <div className="flex flex-wrap mt-4">
          {categories.map((category) => (
            <span
              key={category}
              className="bg-slate-100 text-gray-600 rounded-full px-2 py-1 text-sm mr-2 mb-2"
            >
              {category}
            </span>
          ))}
        </div>

        <div className="mt-4">
          <EditRestaurant restaurant={restaurant} />
        </div>
      </div>
    </div>
  );
};

const RestaurantDishes = ({ dishes }: { dishes: Dish[] }) => {
  return (
    <div className="my-10">
      <div className="flex flex-row justify-between items-center mb-4">
        <h2 className="text-2xl font-bold">Dishes</h2>
        <AddDish />
      </div>
      <DataTable columns={getDishColumnDefs()} data={dishes} />
    </div>
  );
};

const RestaurantOrders = ({ orders }: { orders: Order[] }) => {
  const columnsOrders = getOrderColumnDefs();

  return (
    <div className="my-10">
      <h2 className="text-2xl font-bold mb-4">Orders</h2>
      <DataTable columns={columnsOrders} data={orders} />
    </div>
  );
};

const getRestaurant = (id: string): Restaurant => {
  return {
    id,
    name: "Restaurant Name",
    address: "123 Main St",
    image: "https://picsum.photos/200/200",
    rating: "4.5",
    categories: ["American", "Fast Food"],
    city: "City",
    state: "State",
    pincode: "123456",
  };
};

const getDishes = (id: string): Dish[] => {
  return [
    {
      id: "1",
      name: "Dish 1",
      description: "Dish 1 Description",
      price: 100,
      image: "https://picsum.photos/200/200",
      type: "veg",
      category: "Dish Category",
    },
    {
      id: "2",
      name: "Dish 2",
      description: "Dish 2 Description",
      price: 100,
      image: "https://picsum.photos/200/200",
      type: "veg",
      category: "Dish Category",
    },
    {
      id: "3",
      name: "Dish 3",
      description: "Dish 3 Description",
      price: 100,
      image: "https://picsum.photos/200/200",
      type: "veg",
      category: "Dish Category",
    },
    {
      id: "4",
      name: "Dish 4",
      description: "Dish 4 Description",
      price: 100,
      image: "https://picsum.photos/200/200",
      type: "veg",
      category: "Dish Category",
    },
    {
      id: "5",
      name: "Dish 5",
      description: "Dish 5 Description",
      price: 100,
      image: "https://picsum.photos/200/200",
      type: "veg",
      category: "Dish Category",
    },
    {
      id: "6",
      name: "Dish 6",
      description: "Dish 6 Description",
      price: 100,
      image: "https://picsum.photos/200/200",
      type: "veg",
      category: "Dish Category",
    },
  ];
};

const getOrders = (id: string): Order[] => {
  return [
    {
      id: "1",
      dishes: [
        {
          name: "Dish 1",
          quantity: 1,
        },
        {
          name: "Dish 2",
          quantity: 2,
        },
      ],
      restaurant: "Restaurant 1",
      user: "User 1",
      amount: 100,
      status: "Pending",
      createdAt: "2021-01-01",
    },
    {
      id: "2",
      dishes: [
        {
          name: "Dish 1",
          quantity: 1,
        },
        {
          name: "Dish 2",
          quantity: 2,
        },
      ],
      restaurant: "Restaurant 2",
      user: "User 2",
      amount: 100,
      status: "Pending",
      createdAt: "2021-01-01",
    },
    {
      id: "3",
      dishes: [
        {
          name: "Dish 1",
          quantity: 1,
        },
        {
          name: "Dish 2",
          quantity: 2,
        },
      ],
      restaurant: "Restaurant 3",
      user: "User 3",
      amount: 100,
      status: "Pending",
      createdAt: "2021-01-01",
    },
    {
      id: "4",
      dishes: [
        {
          name: "Dish 1",
          quantity: 1,
        },
        {
          name: "Dish 2",
          quantity: 2,
        },
      ],
      restaurant: "Restaurant 4",
      user: "User 4",
      amount: 100,
      status: "Pending",
      createdAt: "2021-01-01",
    },
    {
      id: "5",
      dishes: [
        {
          name: "Dish 1",
          quantity: 1,
        },
        {
          name: "Dish 2",
          quantity: 2,
        },
      ],
      restaurant: "Restaurant 5",
      user: "User 5",
      amount: 100,
      status: "Pending",
      createdAt: "2021-01-01",
    },
    {
      id: "6",
      dishes: [
        {
          name: "Dish 1",
          quantity: 1,
        },
        {
          name: "Dish 2",
          quantity: 2,
        },
      ],
      restaurant: "Restaurant 6",
      user: "User 6",
      amount: 100,
      status: "Pending",
      createdAt: "2021-01-01",
    },
  ];
};

export default function Restaurant({
  params,
}: {
  params: {
    id: string;
  };
}) {
  const { id } = params;
  const restaurant = getRestaurant(id as string);
  const dishes = getDishes(id as string);
  const orders = getOrders(id as string);

  return (
    <div className="w-full p-10 bg-slate-50">
      <RestaurantInfo restaurant={restaurant} />
      <RestaurantDishes dishes={dishes} />
      <RestaurantOrders orders={orders} />
    </div>
  );
}
