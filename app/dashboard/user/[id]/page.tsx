"use client";

import { Button } from "@/components/ui/button";
import { DataTable } from "@/components/ui/dataTable";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { ColumnDef } from "@tanstack/react-table";
import { MoreHorizontal } from "lucide-react";
import Link from "next/link";

interface User {
  id: string;
  name: string;
  address: string;
  mobile: string;
}

interface Order {
  id: string;
  dishes: {
    name: string;
    quantity: number;
  }[];
  restaurant: string;
  user: string;
  amount: number;
  status: string;
  createdAt: string;
}

const getOrderColumnDefs = () => {
  const orderColumns: ColumnDef<Order>[] = [
    {
      accessorKey: "dishes",
      header: "Dishes : Quantity",
      cell: ({ row }) => {
        return (
          <div className="flex flex-col gap-2">
            {row.original.dishes.map((dish, index) => (
              <div className="flex items-center gap-2 pb-1" key={index}>
                {dish.name}:<span className="font-bold">{dish.quantity}</span>
              </div>
            ))}
          </div>
        );
      },
    },
    {
      accessorKey: "restaurant",
      header: "Restaurant",
      cell: ({ row }) => {
        return (
          <Link
            href={`/dashboard/restaurant/${row.original.restaurant}`}
            className="text-blue-500 p-1 border-b border-blue-500 cursor-pointer"
          >
            {row.original.restaurant}
          </Link>
        );
      },
    },
    {
      accessorKey: "amount",
      header: "Amount",
      cell: ({ row }) => {
        return <span>{row.original.amount}</span>;
      },
    },
    {
      accessorKey: "status",
      header: "Status",
      cell: ({ row }) => {
        return <span>{row.original.status}</span>;
      },
    },
    {
      accessorKey: "createdAt",
      header: "Created At",
      cell: ({ row }) => {
        return <span>{row.original.createdAt}</span>;
      },
    },
  ];

  return orderColumns;
};

const UserInfo = ({ user }: { user: User }) => {
  const { id, name, address, mobile } = user;

  return (
    <div className="bg-white rounded-lg shadow-md flex flex-row flex-wrap w-3/4">
      <div className="p-6 min-w-64">
        <h2 className="text-xl font-bold mb-2">{name}</h2>
        <p className="text-gray-500 mb-2">{address}</p>
        <p className="text-gray-500 mb-2">Mobile: {mobile}</p>
      </div>
    </div>
  );
};

const UserOrders = ({ orders }: { orders: Order[] }) => {
  const columnsOrders = getOrderColumnDefs();

  return (
    <div className="my-10">
      <h2 className="text-2xl font-bold mb-4">Orders</h2>
      <DataTable columns={columnsOrders} data={orders} />
    </div>
  );
};

const getUser = (id: string): User => {
  return {
    id: "1",
    name: "User 1",
    address: "Address 1",
    mobile: "1234567890",
  };
};

const getOrders = (id: string): Order[] => {
  return [
    {
      id: "1",
      dishes: [
        {
          name: "Dish 1",
          quantity: 1,
        },
        {
          name: "Dish 2",
          quantity: 2,
        },
      ],
      restaurant: "Restaurant 1",
      user: "User 1",
      amount: 100,
      status: "Pending",
      createdAt: "2021-01-01",
    },
    {
      id: "2",
      dishes: [
        {
          name: "Dish 1",
          quantity: 1,
        },
        {
          name: "Dish 2",
          quantity: 2,
        },
      ],
      restaurant: "Restaurant 2",
      user: "User 2",
      amount: 100,
      status: "Pending",
      createdAt: "2021-01-01",
    },
    {
      id: "3",
      dishes: [
        {
          name: "Dish 1",
          quantity: 1,
        },
        {
          name: "Dish 2",
          quantity: 2,
        },
      ],
      restaurant: "Restaurant 3",
      user: "User 3",
      amount: 100,
      status: "Pending",
      createdAt: "2021-01-01",
    },
    {
      id: "4",
      dishes: [
        {
          name: "Dish 1",
          quantity: 1,
        },
        {
          name: "Dish 2",
          quantity: 2,
        },
      ],
      restaurant: "Restaurant 4",
      user: "User 4",
      amount: 100,
      status: "Pending",
      createdAt: "2021-01-01",
    },
    {
      id: "5",
      dishes: [
        {
          name: "Dish 1",
          quantity: 1,
        },
        {
          name: "Dish 2",
          quantity: 2,
        },
      ],
      restaurant: "Restaurant 5",
      user: "User 5",
      amount: 100,
      status: "Pending",
      createdAt: "2021-01-01",
    },
    {
      id: "6",
      dishes: [
        {
          name: "Dish 1",
          quantity: 1,
        },
        {
          name: "Dish 2",
          quantity: 2,
        },
      ],
      restaurant: "Restaurant 6",
      user: "User 6",
      amount: 100,
      status: "Pending",
      createdAt: "2021-01-01",
    },
  ];
};

export default function Restaurant({
  params,
}: {
  params: {
    id: string;
  };
}) {
  const { id } = params;
  const user = getUser(id as string);
  const orders = getOrders(id as string);

  return (
    <div className="w-full p-10 bg-slate-50">
      <UserInfo user={user} />
      <UserOrders orders={orders} />
    </div>
  );
}
