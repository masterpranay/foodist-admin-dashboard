"use client";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import {
  sendOtp,
  setOtpBufferTime,
  verifyOtp,
} from "@/lib/features/loginSlice";
import { fetchRestaurant } from "@/lib/features/restaurantSlice";
import {
  useAppDispatch,
  useAppSelector,
  useAppThunkDispatch,
} from "@/lib/hooks";
import { Loader2 } from "lucide-react";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";

const OtpForm = ({
  otp,
  setOtp,
  setShowOtpBox,
}: {
  otp: string;
  setOtp: (otp: string) => void;
  setShowOtpBox: (showOtpBox: boolean) => void;
}) => {
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const otpVerifyStatus = useAppSelector(
    (state) => state.login.otpVerifyStatus
  );
  const mobileNumber = useAppSelector((state) => state.login.mobileNumber);
  const role = useAppSelector((state) => state.login.role);
  const userId = useAppSelector((state) => state.login.id);

  const ownerRestaurantStatus = useAppSelector(
    (state) => state.restaurant.status
  );

  const dispatchThunk = useAppThunkDispatch();
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (otpVerifyStatus == "success") {
      setLoading(false);
      dispatch(setOtpBufferTime(0));

      if (role == "owner") {
        dispatchThunk(
          fetchRestaurant({
            userId,
          })
        );
        setLoading(true);
        return;
      }

      router.push("/dashboard");
    }
    if (otpVerifyStatus == "failed") {
      setLoading(false);
    }
  }, [otpVerifyStatus]);

  useEffect(() => {
    if (ownerRestaurantStatus == "success") {
      router.push("/dashboard");
    } else if (ownerRestaurantStatus == "failed") {
      setLoading(false);
    }
  }, [ownerRestaurantStatus]);

  return (
    <form className="flex flex-col gap-4 mt-12 w-80">
      <Label htmlFor="otp" className="text-slate-600">
        OTP
      </Label>
      <Input
        placeholder="Enter OTP"
        className=""
        name="otp"
        type="number"
        value={otp}
        onChange={(e) => {
          setOtp(e.target.value);
        }}
        maxLength={4}
      />
      <Button
        className="bg-[#01b460] hover:bg-[#01a256]"
        onClick={(e) => {
          e.preventDefault();
          if (otp.length != 4) return;
          dispatchThunk(
            verifyOtp({
              otp,
              mobileNumber,
            })
          );
          setLoading(true);
        }}
      >
        {loading && <Loader2 className="w-6 h-6 animate-spin" />}
        {!loading && "Verify OTP"}
      </Button>
    </form>
  );
};

const MobileForm = ({
  mobile,
  setMobile,
  setShowOtpBox,
}: {
  mobile: string;
  setMobile: (mobile: string) => void;
  setShowOtpBox: (showOtpBox: boolean) => void;
}) => {
  const dispatchThunk = useAppThunkDispatch();
  const dispatch = useAppDispatch();
  const [loading, setLoading] = useState(false);
  const otpSendStatus = useAppSelector((state) => state.login.otpSendStatus);

  useEffect(() => {
    if (otpSendStatus == "success") {
      setLoading(false);
      setShowOtpBox(true);
      dispatch(setOtpBufferTime(60));
    }
    if (otpSendStatus == "failed") {
      setLoading(false);
    }
  }, [otpSendStatus]);

  return (
    <form className="flex flex-col gap-4 mt-12 w-80">
      <Label htmlFor="mobile" className="text-slate-600">
        Mobile Number
      </Label>
      <Input
        placeholder="Enter Mobile Number"
        className=""
        name="mobile"
        type="number"
        value={mobile}
        onChange={(e) => {
          setMobile(e.target.value);
        }}
      />
      <Button
        className="bg-[#01b460] hover:bg-[#01a256]"
        onClick={(e) => {
          e.preventDefault();
          if (mobile.length != 10) return;

          setLoading(true);
          dispatchThunk(
            sendOtp({
              mobileNumber: mobile,
            })
          );
        }}
      >
        {loading && <Loader2 className="w-6 h-6 animate-spin" />}
        {!loading && "Send OTP"}
      </Button>
    </form>
  );
};

const SignIn = () => {
  const [mobile, setMobile] = useState("");
  const [otp, setOtp] = useState("");
  const [showOtpBox, setShowOtpBox] = useState(false);

  return (
    <div className="flex flex-col gap-2 items-center my-auto">
      <h1 className="text-3xl font-bold text-[#01b460] hover:bg-[#01a256]">
        Welcome Back!!
      </h1>
      <p className="text-slate-400">Login as the admin of the Foodist.</p>

      {!showOtpBox && (
        <MobileForm
          mobile={mobile}
          setMobile={setMobile}
          setShowOtpBox={setShowOtpBox}
        />
      )}

      {showOtpBox && (
        <OtpForm otp={otp} setOtp={setOtp} setShowOtpBox={setShowOtpBox} />
      )}
    </div>
  );
};

export default function Home() {
  const router = useRouter();
  const loginStatus = useAppSelector((state) => state.login.status);

  useEffect(() => {
    if (loginStatus == "success") {
      router.push("/dashboard");
    }
  }, [loginStatus]);

  return (
    <main className="my-auto h-full w-full flex flex-col">
      <SignIn />
    </main>
  );
}
