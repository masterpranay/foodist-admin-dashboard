"use client";
import { fetchDishesFromLocalStorage } from "@/lib/features/dishSlice";
import { fetchLoginFromLocalStorage } from "@/lib/features/loginSlice";
import { fetchOrdersFromLocalStorage } from "@/lib/features/orderSlice";
import { fetchRestaurantsFromLocalStorage } from "@/lib/features/restaurantSlice";
import { fetchUsersFromLocalStorage } from "@/lib/features/usersSlice";
import { useAppDispatch } from "@/lib/hooks";
import { Pizza } from "lucide-react";
import { useEffect } from "react";

export default function AuthLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(fetchLoginFromLocalStorage());
    dispatch(fetchRestaurantsFromLocalStorage());
    dispatch(fetchDishesFromLocalStorage());
    dispatch(fetchUsersFromLocalStorage());
    dispatch(fetchOrdersFromLocalStorage());
  }, []);

  return (
    <section className="w-full min-h-screen flex flex-row bg-slate-50">
      <section className="w-1/2 px-12 flex flex-col items-center my-auto">
        <Pizza className="text-[#01b460]" size="32px" />
        <h1 className="mt-4 text-4xl font-bold text-[#01b460] flex flex-row items-center">
          Foodist
        </h1>
        <p className="text-lg mt-4 w-96 text-center text-slate-400">
          Your Favourite Food Delivery and Dining App. We are the best in the
          city
        </p>
      </section>

      <section className="w-1/2 bg-white">{children}</section>
    </section>
  );
}
