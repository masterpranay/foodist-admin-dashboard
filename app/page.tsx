"use client";
import { useRouter } from "next/navigation";
import { useEffect } from "react";

export default function Home() {
  const isLoggedIn = false;
  const router = useRouter();

  useEffect(() => {
    const timeOut = setTimeout(() => {
      if (!isLoggedIn) {
        router.push("/auth");
      } else {
        router.push("/dashboard");
      }
    }, 2000);

    return () => clearTimeout(timeOut);
  }, []);

  return (
    <main className="w-full h-screen flex justify-center items-center">
      <h1 className="text-6xl font-extrabold text-[#01b460] animate-pulse">
        Foodist Dashboard
      </h1>
    </main>
  );
}
